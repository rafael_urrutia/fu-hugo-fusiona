const galleryItems = document.querySelectorAll(
  '.project-gallery-container .gallery-item'
);
const body = document.body;
const modalWrapper = document.querySelector('.gallery-modal-wrapper');
const modalSlider = document.querySelector(
  '.gallery-modal-wrapper .gallery-slider'
);
const closeBtn = document.querySelector(
  '.gallery-slider-container .close-modal-btn'
);
const arrowLeft =
  modalWrapper != null
    ? modalWrapper.querySelector('.slick-custom--arrow-prev')
    : '';
const arrowRight =
  modalWrapper != null
    ? modalWrapper.querySelector('.slick-custom--arrow-next')
    : '';

if (modalWrapper != null) {
  $('#gallery-slider').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: false,
    prevArrow: arrowLeft,
    nextArrow: arrowRight,
  });
}

if (modalWrapper != null) {
  galleryItems.forEach((item) => {
    item.addEventListener('click', function () {
      const index = this.getAttribute('item-index');
      // Show modal
      modalWrapper.classList.add('active');
      // block body scroll
      body.style.height = '100vh';
      body.style.overflowY = 'hidden';
      // Set slider
      $('#gallery-slider').slick('slickGoTo', index);
    });
  });

  closeBtn.addEventListener('click', function () {
    body.style.height = 'auto';
    body.style.overflowY = 'scroll';
    modalWrapper.classList.remove('active');
  });
}
