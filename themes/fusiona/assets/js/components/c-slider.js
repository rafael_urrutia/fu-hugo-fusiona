function slidesAnimation(slider, nextSlide) {
  slider
    .find('.js-mask-add-animation')
    .removeClass('o-animation-zoom-in-image o-animation-duration-5');
  slider
    .find('.js-mask-add-animation')
    .eq(nextSlide)
    .addClass('o-animation-zoom-in-image o-animation-duration-5');

  slider
    .find('.js-box-info-add-animation')
    .removeClass('o-animation-fade-in-left');
  slider
    .find('.js-box-info-add-animation')
    .eq(nextSlide)
    .addClass('o-animation-fade-in-left');

  slider
    .find('.js-button-add-animation')
    .removeClass('o-animation-fade-in-up o-animation-delay-15');
  slider
    .find('.js-button-add-animation')
    .eq(nextSlide)
    .addClass('o-animation-fade-in-up o-animation-delay-15');

  slider
    .find('.js-image-add-animation')
    .removeClass('o-animation-fade-in-up o-animation-delay-1');
  slider
    .find('.js-image-add-animation')
    .eq(nextSlide)
    .addClass('o-animation-fade-in-up o-animation-delay-1');
}
