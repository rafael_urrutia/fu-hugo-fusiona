galleryTab = document.querySelector(
  '.tabs-content-container .tab-content[tab-id="gallery"'
);
if (galleryTab != null) {
  if (window.innerWidth > 767 && galleryTab.classList.contains('active')) {
    $('.project-gallery-container').masonry({
      // options
      itemSelector: '.gallery-item',
      transitionDuration: 0,
    });
  }

  window.addEventListener('resize', function () {
    if (window.innerWidth <= 767) {
      $('.project-gallery-container').masonry('destroy');
    } else if (
      window.innerWidth > 767 &&
      galleryTab.classList.contains('active')
    ) {
      $('.project-gallery-container').masonry({
        // options
        itemSelector: '.gallery-item',
        transitionDuration: 0,
      });
    }
  });

  $showMoreBtn = document.querySelector('.show-more-btn');
  $galleryItems = document.querySelectorAll('.gallery-item');
  $showMoreBtn.addEventListener('click', function () {
    $galleryItems.forEach((item, index) => {
      if (index > 1) {
        item.classList.toggle('hide-mob');
      }
    });
    if (this.innerText == 'VER MÁS') {
      this.innerText = 'VER MENOS';
    } else {
      this.innerText = 'VER MÁS';
    }
  });
}
