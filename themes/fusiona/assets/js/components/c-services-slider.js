$('#services-slider').slick({
  lazyLoad: 'ondemand',
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: true,
  arrows: true,
  prevArrow: $('#services-slider')
    .closest('.c-slider__content')
    .find('.slick-custom--arrow-prev'),
  nextArrow: $('#services-slider')
    .closest('.c-slider__content')
    .find('.slick-custom--arrow-next'),
  responsive: [
    {
      breakpoint: sizeTablet,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
      },
    },
    {
      breakpoint: sizeMobile,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        arrows: false,
      },
    },
  ],
});
