const tabs = document.querySelectorAll('.tabs-container .tab');
const tabsContent = document.querySelectorAll(
  '.tabs-content-container .tab-content'
);

tabs.forEach((tab) => {
  tab.addEventListener('click', function () {
    // Deactivate previously tab
    document
      .querySelectorAll(
        '.tabs-container .tab.active, .tabs-content-container .tab-content.active'
      )
      .forEach((e) => {
        e.classList.remove('active');
      });

    // Activate actual tab
    this.classList.add('active');
    const id = this.getAttribute('tab-id');
    tabsContent.forEach((elem) => {
      if (elem.getAttribute('tab-id') == id) {
        elem.classList.add('active');
        if (
          elem.getAttribute('tab-id') == 'gallery' &&
          window.innerWidth > 767
        ) {
          // Set the gallery in projects
          $('.project-gallery-container').masonry({
            // options
            itemSelector: '.gallery-item',
            transitionDuration: 0,
          });
        }
      }
    });
  });
});
