$(document).ready(function () {
  $('#slider-projects').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    arrow: false,
    dots: true,
    centerMode: true,
    centerPadding: '20px',
    responsive: [
      {
        breakpoint: sizeMobile,
        settings: 'unslick',
      },
    ],
  });
});
