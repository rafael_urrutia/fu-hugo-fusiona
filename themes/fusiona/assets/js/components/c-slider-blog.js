$('#slider-blog').slick({
  lazyLoad: 'ondemand',
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  arrows: true,
  prevArrow: $('#slider-blog')
    .closest('.c-slider__content')
    .find('.slick-custom--arrow-prev'),
  nextArrow: $('#slider-blog')
    .closest('.c-slider__content')
    .find('.slick-custom--arrow-next'),
});
