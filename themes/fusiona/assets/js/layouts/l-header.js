const menuButton = document.querySelector('.l-header-hamburger>div');
const header = document.querySelector('.l-header');
const linkAllServices = $('.l-header-menu .animated.has-sub>a');

const itemsWithChildren = document.querySelectorAll(
  '.l-header-menu .navigation ul li.has-sub'
);

itemsWithChildren.forEach((item) => {
  item.addEventListener('click', function () {
    this.classList.toggle('active');
  });
});

menuButton.addEventListener('click', function () {
  header.classList.toggle('is-expand-menu');
});

linkAllServices.on('click', function (e) {
  if (window.matchMedia('(max-width: ' + sizeMobile + 'px)').matches) {
    e.preventDefault();
  }
});
