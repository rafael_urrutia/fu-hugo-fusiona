$(document).ready(function () {
  $('#slider-home-banner')
    .slick({
      lazyLoad: 'ondemand',
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: true,
      prevArrow: $('#slider-home-banner')
        .closest('.c-slider__content')
        .find('.slick-custom--arrow-prev'),
      nextArrow: $('#slider-home-banner')
        .closest('.c-slider__content')
        .find('.slick-custom--arrow-next'),
    })
    .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      slidesAnimation($(this), nextSlide);
    });

  var sliderHomeProjects = $('#slider-home-projects');

  var settingsSliderProject = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: true,
    prevArrow: $('#slider-home-projects')
      .closest('.c-slider__content')
      .find('.slick-custom--arrow-prev'),
    nextArrow: $('#slider-home-projects')
      .closest('.c-slider__content')
      .find('.slick-custom--arrow-next'),
    responsive: [
      {
        breakpoint: sizeMobile,
        settings: 'unslick',
      },
    ],
  };

  function responsiveSliderProject(x) {
    if (x.matches) {
      //MOBILE
      sliderHomeProjects.slick('unslick');
    } else {
      //DESKTOP
      sliderHomeProjects.slick(settingsSliderProject);
    }
  }

  var x = window.matchMedia('(max-width: ' + sizeMobile + 'px)');
  responsiveSliderProject(x);
  x.addListener(responsiveSliderProject);
});

$('.js-expand-lists').on('click', function () {
  $('.p-home__clients').addClass('is-expand');
});
