$(document).ready(function () {
  jQuery.extend(jQuery.validator.messages, {
    required: 'Este campo es obligatorio.',
    remote: 'Por favor, rellena este campo.',
    email: 'Por favor, escribe una dirección de correo válida',
    url: 'Por favor, escribe una URL válida.',
    date: 'Por favor, escribe una fecha válida.',
    dateISO: 'Por favor, escribe una fecha (ISO) válida.',
    number: 'Por favor, escribe un número entero válido.',
    digits: 'Por favor, escribe sólo dígitos.',
    creditcard: 'Por favor, escribe un número de tarjeta válido.',
    equalTo: 'Por favor, escribe el mismo valor de nuevo.',
    accept: 'Por favor, escribe un valor con una extensión aceptada.',
    maxlength: jQuery.validator.format(
      'Por favor, no escribas más de {0} caracteres.'
    ),
    minlength: jQuery.validator.format(
      'Por favor, no escribas menos de {0} caracteres.'
    ),
    rangelength: jQuery.validator.format(
      'Por favor, escribe un valor entre {0} y {1} caracteres.'
    ),
    range: jQuery.validator.format(
      'Por favor, escribe un valor entre {0} y {1}.'
    ),
    max: jQuery.validator.format(
      'Por favor, escribe un valor menor o igual a {0}.'
    ),
    min: jQuery.validator.format(
      'Por favor, escribe un valor mayor o igual a {0}.'
    ),
  });
  $('#form_contacto').validate({
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.appendTo(element.parent('div'));
      //   element.closest('.c-form__group').addClass('error');
    },
    rules: {
      name: {
        required: true,
        minlength: 3,
      },
      email: {
        required: true,
        email: true,
      },
      subject: {
        required: true,
        minlength: 3,
      },
      phone: {
        required: true,
        maxlength: 10,
        number:true,
      },
      message: {
        required: true,
        minlength: 3,
        maxlength: 255,
      },
    },
  }); 

    

  $('#send-contact').click(function () {
    if ($('#form_contacto').valid()) {

    grecaptcha.ready(function() {
        grecaptcha.execute('6Lf1kYQaAAAAAGzxELay-r1JshSXWEe36MycExXR', {action: 'form_contact'}).then(function(token) {
            let tab_active = $('.tab-content.active').attr('tab-id')
            let type = (tab_active == 'chile-contact') ? 1 : 2
            $('#token_recaptcha').val(token)
            $('#type').val(type)
            $('#g-recaptcha-response').val(token)
            let data = $('#form_contacto').serialize();

            $.ajax({
                type: 'POST',
                url: '/api/public/api/contactform',
                data: data,
                success: function (data, textStatus, xhr) {
                if (xhr.status == 200) {
                    $('#form_contacto').trigger('reset');
                    $('.success').css('display', 'block');
                } else {
                    console.error(xhr);
                }
                },
            });
        });
    });
      
    }
  });
});
