function formatNumber(n) {
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}

$('.js-count span').each(function () {
    $(this)
        .prop('Counter', 0)
        .animate({
            Counter: $(this).text(),
        }, {
            duration: 3000,
            easing: 'swing',
            step: function (now) {
                $(this).text(formatNumber(Math.ceil(now)));
            },
        });
});

function parallax(e) {
    if (!sizeMobile.matches) {
        document.querySelectorAll('.object').forEach(function (move) {
            var moving_value = move.getAttribute('data-value');
            var x = (e.clientX * moving_value) / 250;
            var y = (e.clientY * moving_value) / 250;
            move.style.transform = 'translateX(' + x + 'px) translateY(' + y + 'px)';
        });
    }
}

document.addEventListener('mousemove', parallax);

$('#file').change(function (e) {
    var fileExtension = ['docx', 'pdf', 'ppt', 'pptx'];
    if (
        $.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1
    ) {
        $(this).val(null);
        $(this).parent().find('i').text(null);
    } else {
        $(this).parent().find('i').text(e.target.files[0].name);
    }
});

$(document).ready(function () {
    jQuery.extend(jQuery.validator.messages, {
        required: 'Este campo es obligatorio.',
        remote: 'Por favor, rellena este campo.',
        email: 'Por favor, escribe una dirección de correo válida',
        url: 'Por favor, escribe una URL válida.',
        date: 'Por favor, escribe una fecha válida.',
        dateISO: 'Por favor, escribe una fecha (ISO) válida.',
        number: 'Por favor, escribe un número entero válido.',
        digits: 'Por favor, escribe sólo dígitos.',
        creditcard: 'Por favor, escribe un número de tarjeta válido.',
        equalTo: 'Por favor, escribe el mismo valor de nuevo.',
        accept: 'Por favor, escribe un valor con una extensión aceptada.',
        maxlength: jQuery.validator.format(
            'Por favor, no escribas más de {0} caracteres.'
        ),
        minlength: jQuery.validator.format(
            'Por favor, no escribas menos de {0} caracteres.'
        ),
        rangelength: jQuery.validator.format(
            'Por favor, escribe un valor entre {0} y {1} caracteres.'
        ),
        range: jQuery.validator.format(
            'Por favor, escribe un valor entre {0} y {1}.'
        ),
        max: jQuery.validator.format(
            'Por favor, escribe un valor menor o igual a {0}.'
        ),
        min: jQuery.validator.format(
            'Por favor, escribe un valor mayor o igual a {0}.'
        ),
    });

    $('#form_trabaja_con_nosotros').validate({
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.appendTo(element.parent('div'));
            //   element.closest('.c-form__group').addClass('error');
        },
        rules: {
            name: {
                required: true,
                minlength: 3,
            },
            email: {
                required: true,
                email: true,
            },
            file: {
                required: true,
                accept: 'application/msword,application/pdf,application/vnd.ms-powerpoint',
            },
            message: {
                required: true,
                minlength: 3,
                maxlength: 255,
            },
        },
    });

    $('#send').click(function () {
        if ($('#form_trabaja_con_nosotros').valid()) {
            let name = $('#name').val();
            let email = $('#email').val();
            let message = $('#message').val();
            let file = document.querySelector('#file').files[0];
            grecaptcha.ready(function () {
                grecaptcha.execute('6Lf1kYQaAAAAAGzxELay-r1JshSXWEe36MycExXR', {
                    action: 'form_job'
                }).then(function (token) {
                    var formData = new FormData();
                        formData.append('name', name);
                        formData.append('email', email);
                        formData.append('message', message);
                        formData.append('file', file);
                        formData.append('g-recaptcha-response', token);

                        $.ajax({
                            url: '/api/public/api/jobform',
                            type: 'post',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (data, textStatus, xhr) {
                                if (xhr.status == 200) {
                                    $('#form_trabaja_con_nosotros').trigger('reset');
                                    $('.success').css('display', 'block');
                                } else {
                                    console.error(xhr);
                                }
                            },
                        });
                })
            })
            
        }
    });
});

$(document).ready(function () {
    if (window.location.hash) {
        var hash = window.location.hash;
        $('.tab[tab-id="work"]').trigger('click');
        $('html, body').animate({
            scrollTop: $(hash).offset().top - 70
        }, 1000);
    }

    var numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    let divs = $('.p-nosotros__equipo-lists')
        .children()
        .each(function (indexInArray, valueOfElement) {
            numbers = numbers.sort(() => Math.random() - 0.5);
            $(valueOfElement)
                .removeClass()
                .addClass(
                    `object p-nosotros__equipo-list p-nosotros__equipo-list-${numbers[0]}`
                );
            numbers.splice(0, 1);
        });
});