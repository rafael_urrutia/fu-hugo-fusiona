module.exports = {
  plugins: {
    autoprefixer: {
      overrideBrowserslist: ['> 0.5% in US', 'Safari > 9'],
    },
    cssnano: { preset: 'default' },
  },
};
