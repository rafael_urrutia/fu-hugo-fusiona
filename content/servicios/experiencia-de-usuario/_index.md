---
title: Experiencia de Usuario
html_src: templates/servicios-level1.html
breadcrumbs: ['Inicio', 'Servicios', 'Experiencia de Usuario']
banner:
  {
    type: '1',
    title: 'experiencia de usuario',
    titleClass: 'u-text-center',
    subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
    subtitleClass: 'u-text-center',
  }
servicios:
  [
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-1_1.png',
      title: 'USER RESEARCH',
      subtitle: 'Investigar cómo funciona tu negocio y la forma en que los usuarios interactúan con él nos permitirá direccionar tu proyecto para cumplir tus objetivos digitales. Los insights obtenidos en la investigación de usuarios (user research) nos ayudan a definir los perfiles de usuarios y clasificar sus acciones en relación con tu negocio.',
      text: '',
      link:
        {
          text: 'VER DETALLE',
          url: '/servicios/experiencia-de-usuario/user-research/',
        },
    },
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-1_2.png',
      title: 'Consultorías UX',
      subtitle: 'Utilizando nuestra experiencia en levantamiento de información, investigación de usuario, diseño UX y generación de estrategias digitales podemos ayudarte y guiar la toma de decisiones de tu empresa.',
      text: '',
      link:
        {
          text: 'VER DETALLE',
          url: '/servicios/experiencia-de-usuario/consultorias-ux/',
        },
    },
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-1_3.png',
      title: 'Diseño UX',
      subtitle: 'Trabajamos la arquitectura de información de tu sitio web o aplicación basándonos en la data recopilada en el proceso de levantamiento e investigación. Así, desarrollamos proyectos digitales que cumplen con los requerimientos de tus usuarios.',
      text: '',
      link:
        {
          text: 'VER DETALLE',
          url: '/servicios/experiencia-de-usuario/diseno-ux/',
        },
    },
  ]
css_src: []
js_src: []
---
