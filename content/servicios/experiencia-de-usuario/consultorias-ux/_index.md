---
title: Consultorias UX
html_src: templates/servicios-level2.html
breadcrumbs:
  ['Inicio', 'Servicios', 'Experiencia de Usuario', 'Consultorias UX']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Consultorías UX',
    textClass: 'c-banner__text--custom',
    text: 'Te ayudamos a mejorar la experiencia de tus plataformas diseñando una estrategia digital acorde a las necesidades de tus usuarios.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'PERSONALIZADO',
      text: 'Según las necesidades específicas de tu empresa.',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: '100% REMOTO',
      text: 'Adaptable a diferentes modalidades de trabajo.',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'COLABORATIVO',
      text: 'Tu compromiso en este proceso es fundamental para el éxito del proyecto.',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-2_4.png',
          title: 'Levantamiento de información',
          subtitle: 'Nos permite conocer la situación de tu negocio y su canal digital. Identificamos debilidades y te comparamos con la competencia nacional, así como con referentes internacionales. También levantamos un inventario de contenidos y revisamos las métricas de tu plataforma para conocer su estado actual.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-2_1.png',
          title: 'Investigación',
          subtitle: 'Involucramos tanto a usuarios reales como a stakeholders en una investigación que nos ayuda a conocer tu negocio y la industria donde se ubica.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-1_3.png',
          title: 'Definición de estrategia',
          subtitle: 'El aprendizaje obtenido nos ayuda a fijar la estrategia y los pasos a seguir. Definimos cómo debe plantearse tu plataforma y la manera en que el usuario va a interactuar con los contenidos, lo que queda plasmado en los customer journey maps y los perfiles de usuarios.',
          text: '',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---

descripcion:
{
title: 'Descripción del servicio',
list:
[
'El test incluye todas las pantallas y flujos a validar de modo que sea lo más exhaustivo posible. Se contacta usuarios a quienes testear en base a los perfiles. Se establece fecha y hora para aplicación del test de usabilidad.',
'Un moderador aplica el test de usabilidad previamente construido, estando atento a todos los comentarios y consultas que tenga el usuario.<br>Se genera un informe con todos los hallazgos que provengan de los testeos aplicados, de modo de dar una pauta sobre qué medidas tomar para continuar.',
'El test incluye todas las pantallas y flujos a validar de modo que sea lo más exhaustivo posible. Se contacta usuarios a quienes testear en base a los perfiles. Se establece fecha y hora para aplicación del test de usabilidad. La base de datos de usuarios puede provenir del cliente, de Fusiona, o una mezcla de ambos',
],
image: '/img/templates/servicios/level2/img-1.png',
}
