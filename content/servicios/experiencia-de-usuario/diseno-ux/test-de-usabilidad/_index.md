---
title: Test de Usabilidad
html_src: templates/servicios-level3.html
breadcrumbs:
  [
    'Inicio',
    'Servicios',
    'Experiencia de Usuario',
    'Diseño UX',
    'Test de Usabilidad',
  ]
banner:
  {
    type: '1',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Test de Usabilidad',
    textClass: 'c-banner__text--custom',
    subtitle: 'Probamos el comportamiento de los usuarios frente a las plataformas digitales',
  }
descripcion:
  {
    title: '¿Para qué es útil?',
    text:
      [
        'Los testeos de usabilidad permiten conocer los principales hallazgos -positivos y negativos- respecto a la forma en que los usuarios reaccionan ante una interfaz móvil o de escritorio, ofreciendo oportunidades de mejora. También se puede agregar una encuesta para conocer en mayor profundidad a los participantes e indagar en su experiencia a la hora de utilizar las diferentes interfaces.',
        'El test de usabilidad pone a prueba el proyecto y nos entrega información sobre mejoras que podemos implementar previo a la etapa de desarrollo. Nuestro objetivo es validar que los usuarios logren interactuar con las interfaces obteniendo la mejor experiencia posible. Existen diferentes tipos de test y, dependiendo de la necesidad del proyecto, implementaremos uno o varios de ellos para validar la usabilidad según las normativas de Norman Nielsen.',
      ],
    image: '/img/templates/servicios/level3/img-1.svg',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: '100% REMOTO',
      text: 'Este servicio se entrega eficazmente en el contexto de aislamiento social',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'UX LAB',
      text: 'Contamos con infraestructura para el desarrollo de focus y pruebas con usuarios',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'DISEÑO EVOLUTIVO',
      text: 'las soluciones finales son pensadas para ser escalables',
    },
  ]
metodologia:
  {
    title: 'Metodología',
    lists:
      [
        'Guión y pauta de observación: Se establece la pauta que guiará el testeo en base a las diferentes tareas que se necesite probar. Además se hace una pauta de observación que utilizará el observador del test, de modo de encontrar los hallazgos de manera eficaz.',
        'Contactación: Se contacta a los participantes del testeo. Los potenciales participantes se definen en base a perfiles de uso. A los participantes se les ofrece movilización y un incentivo (gift card) por su cooperación. ',
        'Test: Se aplica el test de usabilidad a los participantes contactados. En la dinámica hay un moderador y un observador. Además el cliente puede participar como observador del testeo.',
        'Informe: Se hace una bajada de los hallazgos encontrados en un informe entregable.',
        'Entrega final: Se hace una reunión con el cliente donde se entrega el informe, además de una presentación donde se plantean los hallazgos, propuestas de mejora y distintas conclusiones que hayan surgido a partir del test.',
      ],
    images:
      [
        '/img/templates/servicios/level3/img-grid-1.png',
        '/img/templates/servicios/level3/img-grid-2.png',
        '/img/templates/servicios/level3/img-grid-3.png',
        '/img/templates/servicios/level3/img-grid-4.png',
      ],
  }
herramientas:
  {
    title: 'Herramientas utilizadas',
    lists:
      [
        {
          image: '/img/templates/servicios/level3/img-maze.png',
          title: 'Maze',
          text: '',
        },
        {
          image: '/img/templates/servicios/level3/img-google-slides.png',
          title: 'Lookback',
          text: '',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
