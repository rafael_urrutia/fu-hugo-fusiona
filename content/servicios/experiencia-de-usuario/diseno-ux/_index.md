---
title: Diseño UX
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'Experiencia de Usuario', 'Diseño UX']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Diseño UX',
    textClass: 'c-banner__text--custom',
    text: 'Proponemos un diseño que mejora la experiencia de navegación y es altamente resolutivo.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'DISEÑO EVOLUTIVO',
      text: 'Trabajamos con diseño evolutivo iterando hasta encontrar la mejor opción para tu proyecto digital.',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'DISEÑO CENTRADO EN EL USUARIO',
      text: 'Sus necesidades son el principal pilar de trabajo.',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'TEST DE USABILIDAD',
      text: 'Aplicamos Test de usabilidad para validar el avance del proyecto.',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-5_2.png',
          title: 'Arquitectura de la información',
          subtitle: 'En base a toda la información proveniente del levantamiento y de la definición de la estrategia, construimos el mapa de tu sitio o aplicación y trabajamos en los wireframes que definirán la arquitectura de tu proyecto digital.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-4_1.png',
          title: 'Wireframes',
          subtitle: 'Crearemos wireframes de alta fidelidad en que detallaremos cada flujo y la estructura de cada pantalla. Nuestro objetivo es traducir la estrategia definida previamente en un elemento visual, una etapa crucial previo a la definición visual (diseño UI) del proyecto.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-6_1.png',
          title: 'Test de usabilidad',
          subtitle: 'Realizamos distintos formatos de test de usabilidad dependiendo del tipo de plataforma y los elementos a validar. Los test pueden ser realizados tanto en modalidad presencial como remota.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/experiencia-de-usuario/diseno-ux/test-de-usabilidad',
            },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---

descripcion:
{
title: 'Descripción del servicio',
list:
[
'El test incluye todas las pantallas y flujos a validar de modo que sea lo más exhaustivo posible. Se contacta usuarios a quienes testear en base a los perfiles. Se establece fecha y hora para aplicación del test de usabilidad.',
'Un moderador aplica el test de usabilidad previamente construido, estando atento a todos los comentarios y consultas que tenga el usuario.<br>Se genera un informe con todos los hallazgos que provengan de los testeos aplicados, de modo de dar una pauta sobre qué medidas tomar para continuar.',
'El test incluye todas las pantallas y flujos a validar de modo que sea lo más exhaustivo posible. Se contacta usuarios a quienes testear en base a los perfiles. Se establece fecha y hora para aplicación del test de usabilidad. La base de datos de usuarios puede provenir del cliente, de Fusiona, o una mezcla de ambos',
],
image: '/img/templates/servicios/level2/img-1.png',
}
