---
title: User Research
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'Experiencia de Usuario', 'User Research']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'User Research',
    textClass: 'c-banner__text--custom',
    text: 'Realizamos entrevistas y/o encuestas a usuarios de manera remota o presencial, recolectando insights y sugerencias para trabajar los objetivos de tu proyecto digital.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: '100% ONLINE',
      text: 'Servicio 100% online, se puede realizar para clientes en Chile o en el extranjero.',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'TIEMPO ESTIMADO',
      text: 'El tiempo estimado de este servicio es de 5 a 15 días.',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'INCENTIVOS',
      text: 'Para motivar a los participantes, entregaremos incentivos como giftcards, los cuales se acuerdan con el cliente.',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-1_2.png',
          title: 'Entrevistas con stakeholders',
          subtitle: 'Nos reunimos con los dueños del producto o colaboradores de la empresa  que jugarán un rol importante de cara al proyecto. La cita podría tener la modalidad de workshop, para promover la participación de todos los integrantes y así llegar a conclusiones válidas para la organización en su conjunto.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_3.png',
          title: 'Entrevistas con usuarios',
          subtitle: 'En base a los perfiles definidos, reclutamos a usuarios que serán encuestados o entrevistados -de forma presencial o remota- para recolectar insights que aporten a la investigación. Si ya cuentas con una base de datos de clientes ¡bienvenida sea! Para finalizar haremos un análisis de la data recolectada y te entregaremos un informe con los resultados y conclusiones de la investigación de usuarios.',
          text: '',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---

descripcion:
{
title: 'Descripción del servicio',
list:
[
'El test incluye todas las pantallas y flujos a validar de modo que sea lo más exhaustivo posible. Se contacta usuarios a quienes testear en base a los perfiles. Se establece fecha y hora para aplicación del test de usabilidad.',
'Un moderador aplica el test de usabilidad previamente construido, estando atento a todos los comentarios y consultas que tenga el usuario.<br>Se genera un informe con todos los hallazgos que provengan de los testeos aplicados, de modo de dar una pauta sobre qué medidas tomar para continuar.',
'El test incluye todas las pantallas y flujos a validar de modo que sea lo más exhaustivo posible. Se contacta usuarios a quienes testear en base a los perfiles. Se establece fecha y hora para aplicación del test de usabilidad. La base de datos de usuarios puede provenir del cliente, de Fusiona, o una mezcla de ambos',
],
image: '/img/templates/servicios/level2/img-1.png',
}
