---
title: Servicios
html_src: pages/servicios.html
breadcrumbs: ['Inicio', 'Servicios']
banner:
  {
    type: '6',
    image: '/img/pages/servicios/servicio-banner.png',
    title: 'Nuestros servicios',
    titleClass: '',
    subtitle: 'Análisis, diseño y tecnología que proyectan tu presencia digital y maximizan tu inversión.',
    subtitleClass: '',
    text: 'Una sólida base estratégica y la profunda comprensión de las necesidades de nuestros clientes nos permiten validar propuestas creativas, construir relaciones a largo plazo y garantizar el éxito sostenido de los proyectos.',
  }
informacion:
  {
    title: 'Un conjunto de<br>servicios, a tu medida…',
    text:
      'No todos los clientes tienen los mismos objetivos y sabemos que las prioridades pueden cambiar con el tiempo. Por eso, nuestro acompañamiento digital está diseñado para ser flexible.
      Podemos apoyarte en una o todas estas áreas del marketing digital. Pero, más allá de departamentos o equipos, te proponemos trabajar con profesionales experimentados y comprometidos que colaboran para alcanzar tus metas digitales. ',
  }
servicios:
  [
    {
      link:
        {
          text: 'Experiencia<br>de usuario',
          url: '/servicios/experiencia-de-usuario/',
          ico: '/img/pages/servicios/ico-experiencia-usuario.png',
          class: 'u-color-green1',
        },
    },
    {
      link:
        {
          text: 'Analítica digital',
          url: '/servicios/analitica-digital/',
          ico: '/img/pages/servicios/ico-analitica-digital.png',
          class: 'u-color-orange1',
        },
    },
    {
      link:
        {
          text: 'Desarrollo y<br>tecnología',
          url: '/servicios/desarrollo-y-tecnologia/',
          ico: '/img/pages/servicios/ico-desarrollo-tecnologia.png',
          class: 'u-color-purple3',
        },
    },
    {
      link:
        {
          text: 'Diseño digital',
          url: '/servicios/diseno-digital/',
          ico: '/img/pages/servicios/ico-diseno-digital.png',
          class: 'u-color-gray7',
        },
    },
    {
      link:
        {
          text: 'SEO',
          url: '/servicios/seo/',
          ico: '/img/pages/servicios/ico-seo.png',
          class: 'u-color-blue2',
        },
    },
    {
      link:
        {
          text: 'Mantención',
          url: '/servicios/mantencion/',
          ico: '/img/pages/servicios/ico-mantencion.png',
          class: 'u-color-green1',
        },
    },
    {
      link:
        {
          text: 'Manejo de <br>Activos TI',
          url: '/servicios/manejo-de-activos-ti/',
          ico: '/img/pages/servicios/ico-soporte-informatico.png',
          class: 'u-color-orange1',
        },
    },
  ]
css_src: []
js_src: []
---
