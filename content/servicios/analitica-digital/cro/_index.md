---
title: CRO
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'Analítica Digital', 'CRO']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'CRO',
    textClass: 'c-banner__text--custom',
    text: 'Servicio de análisis de los flujos de conversión del sitio web o app, con el objetivo de identificar las trabas que encuentran los usuarios y las oportunidades de mejora.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'PERSONALIZADO',
      text: 'Personalizado según las necesidades específicas de tu empresa',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: '100% REMOTO',
      text: '100% remoto adaptable a diferentes modalidades de trabajo',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'COLABORATIVO',
      text: 'Colaborativo, tu compromiso en este proceso es fundamental para el éxito del proyecto',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-2_1.png',
          title: 'Auditoría y análisis de datos',
          subtitle: 'Analizamos extensivamente el funnel de conversión y los datos cuantitativos de tu sitio para identificar oportunidades de optimización, potenciales puntos de fuga de usuarios y otras trabas que pueden estar perjudicando la tasa de conversión del sitio. Utilizamos la analítica de datos sumada a heatmaps, scroll maps y grabaciones de sesión para entender mejor el comportamiento del usuario en tu sitio.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-7_1.png',
          title: 'Investigación de la experiencia de usuario',
          subtitle: 'Para saber qué buscan tus usuarios, debemos escucharles de manera exhaustiva. Nuestra investigación de experiencia de usuario proporciona todos los datos cualitativos necesarios para ilustrar y entender el perfil del usuario de tu página web.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-5_4.png',
          title: 'Elaboración de hipótesis',
          subtitle: 'Una vez analizada toda la información de tu sitio llega el momento de plantear una hipótesis de trabajo, establecer metas y hacer el seguimiento de las tasas de conversión.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_2.png',
          title: 'Realización de tests',
          subtitle: 'Ya listados los posibles factores que pueden estar perjudicando la conversión, ponemos en marcha los tests A/B que verificarán las hipótesis generadas anteriormente.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-1_3.png',
          title: 'Implementación de cambios',
          subtitle: 'A la vista de los resultados de los tests es hora de proponer e implementar mejoras que tengan un impacto significativo en la tasa de conversión de la página web o app.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-2_4.png',
          title: 'Análisis de resultados y plan de mejora contínua',
          subtitle: 'Una vez implementadas las mejoras se debe comprobar si están surtiendo efecto, es decir, si conseguimos incrementar la tasa de conversión. Se crea un plan de mejora continua para identificar permanentemente errores u oportunidades de perfeccionamiento.',
          text: '',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
