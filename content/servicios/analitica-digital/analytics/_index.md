---
title: Analytics
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'Analítica Digital', 'Analytics']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Analytics',
    textClass: 'c-banner__text--custom',
    text: 'Servicio de analítica de datos que te ayuda a establecer una estrategia de medición digital  para la toma de mejores decisiones de negocio.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'PERSONALIZADO',
      text: 'Personalizado según las necesidades específicas de tu empresa',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: '100% REMOTO',
      text: '100% remoto adaptable a diferentes modalidades de trabajo',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'COLABORATIVO',
      text: 'Colaborativo, tu compromiso en este proceso es fundamental para el éxito del proyecto',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-2_1.png',
          title: 'Auditoría a las implementaciones analíticas',
          subtitle: 'Nos aseguramos de que los softwares de analítica que usaremos estén correctamente implementados para garantizar la precisión de los datos clave para el proyecto. Utilizamos herramientas como Google Analytics & Firebase, Google Tag Manager, Google Optimize, entre otros.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-5_4.png',
          title: 'Estrategias de medición y definición de KPIs',
          subtitle: 'Desarrollamos un plan de acción de acuerdo a tus capacidades y objetivos de negocio, con foco en el cumplimiento de los KPIs establecidos. Nuestra consultoría es 100% personalizada de acuerdo a tus necesidades, estaremos al lado tuyo durante todas las fases del proyecto, prestándote asesoría en todos los cambios e implementaciones que sean necesarias.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-2_4.png',
          title: 'Reportería de resultados',
          subtitle: 'Diseño y mantención de dashboards interactivos personalizados de acuerdo a cada proyecto, velando siempre por la integridad y la fácil visualización de los datos.',
          text: '',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
