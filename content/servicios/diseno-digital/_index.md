---
title: Diseño Digital
html_src: templates/servicios-level1.html
breadcrumbs: ['Inicio', 'Servicios', 'Diseño Digital']
banner:
  {
    type: '1',
    title: 'Diseño digital',
    titleClass: 'u-text-center',
    subtitle: 'Proponemos diseños que reflejan tu identidad e imagen de marca de manera atractiva y moderna de cara a los usuarios.',
    subtitleClass: 'u-text-center',
  }
servicios:
  [
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-4_1.png',
      title: 'Interfaz de usuario',
      subtitle: 'Definimos cómo se verán los componentes de tu proyecto digital con los cuales tus usuarios van a interactuar directamente. Aplicamos todos los criterios gráficos necesarios para generar una interfaz atractiva que  potencie la usabilidad. Para finalizar, validamos el UI (User Interface o diseño visual de interfaz) con los usuarios, lo que nos entrega hallazgos que pueden transformarse en mejoras y optimización del diseño visual previo a su paso a desarrollo.',
      text: '',
    },
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-4_2.png',
      title: 'Diseño gráfico',
      subtitle: 'En complemento al apoyo estratégico y de experiencia de usuario que brindamos a tu proyecto digital, podemos trabajar en todo el material visual complementario que necesites, como por ejemplo, desarrollar un manual de uso digital de marca o generar gráficas para mailings de promoción. Creamos piezas gráficas complementarias consistentes, que abordamos desde el conocimiento adquirido en las fases previas.',
      text: '',
    },
  ]
css_src: []
js_src: []
---
