---
title: On-page SEO
html_src: templates/servicios-level3.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Auditoría SEO', 'On-page SEO']
banner:
  {
    type: '1',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'On-page SEO',
    textClass: 'c-banner__text--custom',
    subtitle: 'Podemos ayudarte a construir un sitio web amigable para los motores de búsqueda utilizando estrategias On-Page SEO. Estas tácticas sirven como una especie de guía útil para tus usuarios, al facilitarles encontrar e interactuar con el contenido de tu página.',
  }
descripcion:
  {
    title: 'Factores de optimización de SEO On-Page',
    lists:
      [
        '<b>Formato adecuado y uso meta tags estratégicos</b><br>Pocos tienen tiempo para leer grandes bloques de texto, por esa razón las páginas bien formateadas, aparte de ser posicionables, ofrecen a los visitantes un respiro y hacen que su contenido sea más agradable de leer.',
        '<b>Exactitud en el contenido</b><br>El contenido que proporciona evidencias y antecedentes -como información de respaldo y referencias- tiene más valor para los motores de búsqueda en comparación con artículos sin datos que los respalden. En términos generales, es más probable que el contenido más extenso y educativo tenga un mayor rango en Google.',
        '<b>Optimización del contenido visual</b><br>Las imágenes y los videos captan más la atención de los usuarios que los grandes bloques de texto. Sin embargo, también tardan más en cargarse, por lo que el SEO exige una optimización efectiva de los medios visuales.',
      ],
    image: '/img/templates/servicios/level3/img-1.svg',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: '100% REMOTO',
      text: 'Este servicio se entrega eficazmente en el contexto de aislamiento social',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'UX LAB',
      text: 'Contamos con infraestructura para el desarrollo de focus y pruebas con usuarios',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'DISEÑO EVOLUTIVO',
      text: 'las soluciones finales son pensadas para ser escalables',
    },
  ]
metodologia:
  {
    title: 'En nuestra receta SEO On-Page abordamos todo lo anterior con un enfoque basado en estos tres pilares',
    lists:
      [
        '<b>Relevancia</b><br>Cuán relevante (o accesible) es el dominio o una página de destino considerando las palabras clave y el público objetivo.',
        '<b>Autoridad</b><br>Qué tan fuerte es el dominio. Las personas llegarán a conocer tus servicios porque eres considerado un líder en la industria.',
        '<b>Confianza</b><br>Ser un experto y una fuente fidedigna significa que las personas pueden confiar en el sitio para proporcionar información honesta y precisa.',
      ],
    images:
      [
        '/img/templates/servicios/level3/img-grid-1.png',
        '/img/templates/servicios/level3/img-grid-2.png',
        '/img/templates/servicios/level3/img-grid-3.png',
        '/img/templates/servicios/level3/img-grid-4.png',
      ],
  }
herramientas1:
  {
    title: 'Herramientas utilizadas',
    lists:
      [
        {
          image: '/img/templates/servicios/level3/img-google-slides.png',
          title: 'Presentaciones Google',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
        {
          image: '/img/templates/servicios/level3/img-maze.png',
          title: 'Maze',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
