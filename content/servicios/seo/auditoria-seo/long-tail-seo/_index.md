---
title: Long Tail SEO
html_src: templates/servicios-level3.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Auditoría SEO', 'Long Tail SEO']
banner:
  {
    type: '1',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Long Tail SEO',
    textClass: 'c-banner__text--custom',
    subtitle: 'Las palabras clave de cola larga (long tail keywords) son frases de búsqueda muy específicas que sirven para identificar la intención de búsqueda del usuario y tienen tanto un volumen de búsqueda como una competencia baja, lo que genera tasas de conversión generalmente altas. Por esta razón son también mucho más efectivas al momento de determinar la intención de compra.',
  }
descripcion:
  {
    title: 'Caracteristicas',
    lists:
      [
        '<b>Aumento de relevancia</b><br>Cuanto más relevantes sean las palabras clave de tu producto o servicio, mayores serán las posibilidades de que los visitantes que encuentren el sitio se conviertan en clientes.',
        '<b>Baja competencia</b><br>Una breve investigación de palabras clave de cola larga puede ser muy útil para identificar las consultas que denoten intención de compra y complementar los esfuerzos de marketing de contenido para reforzar esos conceptos.',
        '<b>Incremento de conversiones</b><br>Al ser específicas para tu negocio y nicho, las palabras clave de cola larga son más fáciles de clasificar -y por lo mismo de explotar- para mejorar las conversiones.',
      ],
    image: '/img/templates/servicios/level3/img-1.svg',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: '100% REMOTO',
      text: 'Este servicio se entrega eficazmente en el contexto de aislamiento social',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'UX LAB',
      text: 'Contamos con infraestructura para el desarrollo de focus y pruebas con usuarios',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'DISEÑO EVOLUTIVO',
      text: 'las soluciones finales son pensadas para ser escalables',
    },
  ]
metodologia1:
  {
    title: 'Metodología',
    lists:
      [
        '<strong>Prospección</strong><br>Utilizamos tanto el análisis de la competencia como los operadores de motores de búsqueda para encontrar los sitios objetivo correctos para obtener backlinks.',
        '<strong>Verificación</strong><br>Empleamos estrictos criterios de verificación para asegurarnos que las métricas y la calidad del sitio web cumplan con nuestros estándares. Nos centramos especialmente en el tráfico orgánico, la relevancia y la autoridad del dominio.',
        '<strong>Alcance</strong><br>Nos acercamos a nuestra lista de sitios web investigados para explorar una oportunidad de ganar un backlink y adaptar nuestro enfoque con cada campaña.',
        '<strong>Backlinks</strong><br>Podemos obtener backlinks al compartir con terceros un recurso valioso de tu sitio web, un contenido específico creado con este fin u otras formas de agregar valor.',
      ],
    images:
      [
        '/img/templates/servicios/level3/img-grid-1.png',
        '/img/templates/servicios/level3/img-grid-2.png',
        '/img/templates/servicios/level3/img-grid-3.png',
        '/img/templates/servicios/level3/img-grid-4.png',
      ],
  }
herramientas1:
  {
    title: 'Herramientas utilizadas',
    lists:
      [
        {
          image: '/img/templates/servicios/level3/img-google-slides.png',
          title: 'Presentaciones Google',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
        {
          image: '/img/templates/servicios/level3/img-maze.png',
          title: 'Maze',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
