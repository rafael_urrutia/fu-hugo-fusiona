---
title: SEO Técnico
html_src: templates/servicios-level3.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Auditoría SEO', 'SEO Técnico']
banner:
  {
    type: '1',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'SEO Técnico',
    textClass: 'c-banner__text--custom',
    subtitle: 'Un SEO exitoso parte por una sólida base técnica que garantice que tu web está optimizada para responder a los constantes cambios de algoritmo de los buscadores.',
  }
descripcion1:
  {
    title: '¿Qué hacemos?',
    lists:
      [
        '<b>Gestión de links</b><br>Una estrategia centrada en identificar y reemplazar enlaces rotos en dominios semánticamente relevantes y altamente autorizados.',
        '<b>Anchor Text optimization</b><br>Dentro del proceso de construcción de enlaces, y en base a nuestro proceso de keyword research, optimizamos los vínculos para que tengan los mejores textos que inviten al clic.',
        '<b>Page-Specific backlinks</b><br>La combinación de tecnología y personas nos facilita la obtención de enlaces hiper dirigidos -con métricas aprobadas por Google- desde nuevos dominios de referencia a cualquier página de tu sitio web.',
      ],
    image: '/img/templates/servicios/level3/img-1.svg',
  }
blockServices:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'ENCONTRABLES',
      text: 'Optimizamos tus activos digitales para que los contenidos sean encontrables tanto por personas como bots.',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'MOBILE FIRST',
      text: 'Nuestras propuestas de mejora son siempre mobile first.',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'REGLAS DE REDIRECCIONAMIENTO',
      text: 'Optimizamos URLs e  implementamos reglas de redireccionamiento estratégicas y seguras para la comunicación con servidores.',
    },
  ]
metodologia:
  {
    title: 'METODOLOGÍA',
    text:
      [
        'Analizamos si tu sitio sigue las pautas y los requisitos de los motores de búsqueda en las etapas de rastreo,  indexación, renderización y experiencia de usuario',
      ],
    lists:
      [
        '<b>Rastreo, indexación & rendering</b><br>De la capacidad de rastreo (crawling) de un sitio web depende qué tan visible es una URL para los motores de búsqueda. El contenido duplicado, redirecciones y declaraciones canónicas son elementos que atentan contra la indexación e impiden que el sitio aparezca en los resultados de búsqueda (SERP). <br>Por otro lado, la renderización -generalmente asociada a optimizar elementos javascript dentro del sitio- permite asegurarnos que el contenido sea visible y rápidamente evaluado por los motores de búsqueda.',
        '<b>Usability</b><br>Los aspectos de usabilidad, funcionalidad y diseño de un sitio web influyen en su clasificación y determinan cómo actuarán los usuarios al visitarlo.',
        '<b>Mobile Optimization</b><br>La indexación <b>mobile-first</b> es la prioridad de Google, la línea de base para entender cómo el motor de búsqueda clasifica los sitios. Las versiones para dispositivos móviles, sobre todo aquellas mejoradas según la intención del usuario, son siempre mejor calificadas respecto a las versiones de escritorio.',
        '<b>Network Performance</b><br>Los sitios web y las páginas web que tienen problemas de accesibilidad aún pueden clasificarse, pero a menudo se penalizan en los resultados de búsqueda. Podemos guiarte para garantizar que tu sitio web se procesa adecuadamente al verificar elementos como el rendimiento del servidor y la seguridad HTTPS.',
      ],
    images:
      [
        '/img/templates/servicios/level3/img-grid-1.png',
        '/img/templates/servicios/level3/img-grid-2.png',
        '/img/templates/servicios/level3/img-grid-3.png',
        '/img/templates/servicios/level3/img-grid-4.png',
      ],
  }
herramientas1:
  {
    title: 'Herramientas utilizadas',
    lists:
      [
        {
          image: '/img/templates/servicios/level3/img-google-slides.png',
          title: 'Presentaciones Google',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
        {
          image: '/img/templates/servicios/level3/img-maze.png',
          title: 'Maze',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
