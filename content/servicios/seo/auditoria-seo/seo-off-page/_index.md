---
title: SEO Off-Page
html_src: templates/servicios-level3.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Auditoría SEO', 'SEO Off-Page']
banner:
  {
    type: '1',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'SEO Off-Page',
    textClass: 'c-banner__text--custom',
    subtitle: 'La optimización de tu página web es más que simplemente usar las palabras clave correctas en el contenido. Lo que sucede "off-page" es igual de importante, si no más. El SEO off-page abarca todas las estrategias de marketing digital fuera de tu propio sitio web que te ayudan a obtener un mayor puntaje en SERP aumentando la visibilidad de tu marca.',
  }
descripcion:
  {
    title: '¿Qué hacemos?',
    lists:
      [
        '<b>Gestión de links</b><br>Una estrategia centrada en identificar y reemplazar enlaces rotos en dominios semánticamente relevantes y altamente autorizados.',
        '<b>Anchor Text optimization</b><br>Dentro del proceso de construcción de enlaces, y en base a nuestro proceso de keyword research, optimizamos los vínculos para que tengan los mejores textos que inviten al clic.',
        '<b>Page-Specific backlinks</b><br>La combinación de tecnología y personas nos facilita la obtención de enlaces hiper dirigidos -con métricas aprobadas por Google- desde nuevos dominios de referencia a cualquier página de tu sitio web.',
      ],
    image: '/img/templates/servicios/level3/img-1.svg',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: '100% REMOTO',
      text: 'Este servicio se entrega eficazmente en el contexto de aislamiento social',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'UX LAB',
      text: 'Contamos con infraestructura para el desarrollo de focus y pruebas con usuarios',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'DISEÑO EVOLUTIVO',
      text: 'las soluciones finales son pensadas para ser escalables',
    },
  ]
metodologia1:
  {
    title: 'En nuestra receta SEO On-Page abordamos todo lo anterior con un enfoque basado en estos tres pilares',
    lists:
      [
        '<b>Relevancia</b><br>Cuán relevante (o accesible) es el dominio o una página de destino considerando las palabras clave y el público objetivo.',
        '<b>Autoridad</b><br>Qué tan fuerte es el dominio. Las personas llegarán a conocer tus servicios porque eres considerado un líder en la industria.',
        '<b>Confianza</b><br>Ser un experto y una fuente fidedigna significa que las personas pueden confiar en el sitio para proporcionar información honesta y precisa.',
      ],
    images:
      [
        '/img/templates/servicios/level3/img-grid-1.png',
        '/img/templates/servicios/level3/img-grid-2.png',
        '/img/templates/servicios/level3/img-grid-3.png',
        '/img/templates/servicios/level3/img-grid-4.png',
      ],
  }
herramientas1:
  {
    title: 'Herramientas utilizadas',
    lists:
      [
        {
          image: '/img/templates/servicios/level3/img-google-slides.png',
          title: 'Presentaciones Google',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
        {
          image: '/img/templates/servicios/level3/img-maze.png',
          title: 'Maze',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
