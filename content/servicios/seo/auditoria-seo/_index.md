---
title: Auditoría SEO
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Auditoría SEO']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Auditoría SEO',
    textClass: 'c-banner__text--custom',
    text: 'Investigamos en profundidad tu web y entregamos un reporte con hallazgos y un paso a paso de medidas a implementar por los responsables técnicos y editoriales de la plataforma.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'PERSONALIZADO',
      text: 'Personalizado según las necesidades específicas de tu empresa',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: '100% REMOTO',
      text: '100% remoto adaptable a diferentes modalidades de trabajo',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'COLABORATIVO',
      text: 'Colaborativo, tu compromiso en este proceso es fundamental para el éxito del proyecto',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_1.png',
          title: 'On-page SEO',
          subtitle: 'Optimizamos las páginas webs individuales para aumentar su visibilidad en los motores de búsqueda. Con las acciones SEO On-page hacemos que tus usuarios encuentren fácilmente el contenido y puedan interactuar con ellos de manera amigable.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/auditoria-seo/on-page-seo',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_2.png',
          title: 'SEO Técnico',
          subtitle: 'A través del SEO técnico mejoramos la infraestructura de tu sitio web para permitir que los motores de búsqueda rastreen, indexen y puedan renderizar tus páginas correctamente.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/auditoria-seo/seo-tecnico',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_3.png',
          title: 'SEO Off-Page',
          subtitle: 'Utilizamos las mejores prácticas de SEO Off-page para que obtengas gran exposición de tu sitio en plataformas de terceros. Además, evitamos penalizaciones que pueden ir surgiendo mes a mes por links no asociados a tu marca o negocio.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/auditoria-seo/seo-off-page',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-5_1.png',
          title: 'Long Tail SEO',
          subtitle: 'Trabajamos las palabras clave de cola larga (long tail), aquellas frases específicas que sirven para entender la intención de búsqueda de los usuarios -con un volumen y competencia bajos- lo que genera tasas de conversión generalmente altas.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/auditoria-seo/long-tail-seo',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-6_1.png',
          title: 'Link Building',
          subtitle: 'Para superar a tu competencia fortaleceremos la autoridad de tu sitio. Esto se puede lograr ganando enlaces de sitios web que apunten a tu sitio, construyendo activos en línea y nutriendo a una audiencia que ayudará a amplificar el contenido.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/auditoria-seo/link-building',
            },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
