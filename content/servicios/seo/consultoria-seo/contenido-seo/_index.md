---
title: Contenido SEO
html_src: templates/servicios-level3.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Consultoría SEO', 'Contenido SEO']
banner:
  {
    type: '1',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Contenido SEO',
    textClass: 'c-banner__text--custom',
    subtitle: 'Dado que el contenido sigue siendo una de los pilares más sólidos para Google, su optimización constante es clave para clasificar en las SERP.',
  }
descripcion:
  {
    title: '¿Cómo?',
    text:
      [
        'Optimizamos tu estrategia de contenido para centrarnos en palabras clave y búsqueda de intención de los usuarios. Además, incorporamos las mejores prácticas de diseño UX para asegurarnos de que tu contenido se vea genial tanto para las personas como para los motores de búsqueda. Por último, maximizamos el engagement y las conversiones con llamados a la acción orientados según tus KPI’s prioritarios',
      ],
    image: '/img/templates/servicios/level3/img-1.svg',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: '100% REMOTO',
      text: 'Este servicio se entrega eficazmente en el contexto de aislamiento social',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'UX LAB',
      text: 'Contamos con infraestructura para el desarrollo de focus y pruebas con usuarios',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'DISEÑO EVOLUTIVO',
      text: 'las soluciones finales son pensadas para ser escalables',
    },
  ]
metodologia1:
  {
    title: 'Metodología',
    lists:
      [
        '<strong>Prospección</strong><br>Utilizamos tanto el análisis de la competencia como los operadores de motores de búsqueda para encontrar los sitios objetivo correctos para obtener backlinks.',
        '<strong>Verificación</strong><br>Empleamos estrictos criterios de verificación para asegurarnos que las métricas y la calidad del sitio web cumplan con nuestros estándares. Nos centramos especialmente en el tráfico orgánico, la relevancia y la autoridad del dominio.',
        '<strong>Alcance</strong><br>Nos acercamos a nuestra lista de sitios web investigados para explorar una oportunidad de ganar un backlink y adaptar nuestro enfoque con cada campaña.',
        '<strong>Backlinks</strong><br>Podemos obtener backlinks al compartir con terceros un recurso valioso de tu sitio web, un contenido específico creado con este fin u otras formas de agregar valor.',
      ],
    images:
      [
        '/img/templates/servicios/level3/img-grid-1.png',
        '/img/templates/servicios/level3/img-grid-2.png',
        '/img/templates/servicios/level3/img-grid-3.png',
        '/img/templates/servicios/level3/img-grid-4.png',
      ],
  }
herramientas1:
  {
    title: 'Herramientas utilizadas',
    lists:
      [
        {
          image: '/img/templates/servicios/level3/img-google-slides.png',
          title: 'Presentaciones Google',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
        {
          image: '/img/templates/servicios/level3/img-maze.png',
          title: 'Maze',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
