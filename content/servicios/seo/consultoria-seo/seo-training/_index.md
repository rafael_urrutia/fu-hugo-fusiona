---
title: SEO Training
html_src: templates/servicios-level3.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Consultoría SEO', 'SEO Training']
banner:
  {
    type: '1',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'SEO Training',
    textClass: 'c-banner__text--custom',
    subtitle: 'Aprende, comprende y aplica SEO con una formación adaptada para tu negocio.',
  }
descripcion:
  {
    title: 'Un poco de contexto',
    text:
      [
        'Los motores de búsqueda son responsables de generar alrededor del 70% de todo el tráfico web, lo que hace que el SEO sea un componente esencial de cualquier estrategia de marketing digital. <br>Nuestros cursos, talleres o capacitaciones cubren todos los aspectos de la búsqueda orgánica, desde SEO técnico y creación de contenido hasta la optimización de YouTube y el SEO local.',
      ],
    image: '/img/templates/servicios/level3/img-1.svg',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: '100% REMOTO',
      text: 'Este servicio se entrega eficazmente en el contexto de aislamiento social',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'UX LAB',
      text: 'Contamos con infraestructura para el desarrollo de focus y pruebas con usuarios',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'DISEÑO EVOLUTIVO',
      text: 'las soluciones finales son pensadas para ser escalables',
    },
  ]
metodologia:
  {
    title: 'Temas de nuestro entrenamiento SEO',
    lists:
      [
        'Identificación de oportunidades para el desarrollo de contenidos',
        'El Internet desde la perspectiva de mercado',
        'El rol de los buscadores o motores de búsqueda',
        'Las acciones en Internet: Atracción/Conversión/Engagement',
        'Canales de atracción de visitas a un sitio',
        'SEO & SEM',
        '¿Qué determina la posición de una página en los resultados de búsqueda?',
        'Expectativas versus realidad en los cambios de los rankings',
        'Procesos de crawlability e indexación',
        'Arquitectura y URL para sitios de ecommerce',
        'Concepto de silos en sitios de ecommerce',
        'Identificación de las áreas editoriales SEO',
        'Edición de title y meta descripción',
        'Contenidos: cómo editar para los buscadores',
        'Acerca de la duplicación de contenidos y los gatillantes',
        'Datos estructurados: generales y de ecommerce',
        'Optimización de imágenes y videos',
        'Principios de SEO internacional',
        'Principios de la fuerza de posicionamiento y cómo administrarlo',
        'Nociones de la velocidad de carga de una página',
        'Principales KPIs asociados a la gestión de SEO',
      ],
    text:
      [
        'Al final del curso se entregará un PDF con la documentación de los contenidos del SEO Trainning.',
      ],
    images:
      [
        '/img/templates/servicios/level3/img-grid-1.png',
        '/img/templates/servicios/level3/img-grid-2.png',
        '/img/templates/servicios/level3/img-grid-3.png',
        '/img/templates/servicios/level3/img-grid-4.png',
      ],
  }
herramientas1:
  {
    title: 'Herramientas utilizadas',
    lists:
      [
        {
          image: '/img/templates/servicios/level3/img-google-slides.png',
          title: 'Presentaciones Google',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
        {
          image: '/img/templates/servicios/level3/img-maze.png',
          title: 'Maze',
          text: 'Se arma con Presentaciones de Google. Se entrega en PDF.',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
