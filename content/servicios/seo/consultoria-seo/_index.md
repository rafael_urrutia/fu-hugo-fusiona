---
title: Consultoría SEO
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Consultoría SEO']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Consultoría SEO',
    textClass: 'c-banner__text--custom',
    text: 'Construimos una planificación SEO que  impulsará tu rendimiento orgánico y aumentará los ingresos de tu negocio.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'PERSONALIZADO',
      text: 'Personalizado según las necesidades específicas de tu empresa',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: '100% REMOTO',
      text: '100% remoto adaptable a diferentes modalidades de trabajo',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'COLABORATIVO',
      text: 'Colaborativo, tu compromiso en este proceso es fundamental para el éxito del proyecto',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-6_2.png',
          title: 'Voice SEO',
          subtitle: 'Las búsquedas de voz siguen masificándose en los sistemas de asistencia personal. Los usuarios recurren cada vez más a Siri, OK Google o Alexa para hacer llamadas, reproducir música, preguntar direcciones o realizar consultas de búsqueda. Lo hacen con una intención clara y esperando  resultados inmediatos. Nosotros podemos optimizar la búsqueda por voz para que tu marca les de lo que necesitan.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/consultoria-seo/voice-seo',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-6_4.png',
          title: 'Contenido SEO',
          subtitle: 'Ofrecemos servicios de marketing de contenido para impulsar el SEO de tu sitio web con un crecimiento orgánico y sostenible. El contenido se encuentra en el corazón de las campañas de marketing digital más exitosas y su ejecución con perspectiva SEO potencia blogs, imágenes, videos y redes sociales.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/consultoria-seo/contenido-seo',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_3.png',
          title: 'SEO training',
          subtitle: 'La omnicanalidad de los negocios y la constante evolución tecnológica requieren un alto conocimiento técnico SEO. A través de talleres y capacitaciones ampliamos y guiamos el conocimiento de tu equipo en esta materia.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/consultoria-seo/seo-training',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_3.png',
          title: 'Ecommerce SEO',
          subtitle: 'Con la correcta estrategia una tienda en línea puede generar un mayor tráfico orgánico, incluso cuando tiene que competir con marcas más grandes y poderosas. Optimizamos los recursos de tus campañas y las hacemos más efectivas con un trabajo conjunto de SEO y SEM para tu ecommerce.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-5_2.png',
          title: 'Mobile SEO',
          subtitle: 'El auge de los teléfonos inteligentes ha puesto el foco en el SEO móvil. Desde septiembre de 2020 el bot de Google a cargo de analizar los sitios web se basa en un dispositivo mobile, por lo que optimizar tu sitio para SEO mobile es una tarea clave que nos puedes obviar.',
          text: '',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
