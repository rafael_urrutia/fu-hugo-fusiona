---
title: Acompañamiento SEO
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Acompañamiento SEO']
banner:
  {
    type: '2',
    image: '/img/templates/servicios/level2/banner-consultorias.svg',
    title: 'Acompañamiento SEO',
    textClass: 'c-banner__text--custom',
    text: 'Asesorías personalizadas para mejorar el posicionamiento de tu negocio en buscadores.',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'PERSONALIZADO',
      text: 'Personalizado según las necesidades específicas de tu empresa',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: '100% REMOTO',
      text: '100% remoto adaptable a diferentes modalidades de trabajo',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'COLABORATIVO',
      text: 'Colaborativo, tu compromiso en este proceso es fundamental para el éxito del proyecto',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_2.png',
          title: 'Estrategia SEO',
          subtitle: 'Ajustamos el contenido de tu web para que las páginas puedan ser indexadas y renderizadas por los motores de búsqueda. Esto significa contenido más relevante en el momento preciso para ser encontrado por los buscadores y crawlers que visitan tu sitio.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/acompanamiento-seo/estrategia-seo',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-5_1.png',
          title: 'Keyword research',
          subtitle: 'Nuestros expertos realizan un levantamiento a los términos que tendrán mayor impacto en tus campañas SEO /SEM a través de una investigación de palabras clave.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/acompanamiento-seo/keyword-research',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_1.png',
          title: 'SEO Local y SEO Internacional',
          subtitle: 'Nos especializamos en  servicios competitivos dentro del área geográfica de tus operaciones. <br>Planificamos e implementamos acciones para ganar tanto para el posicionamiento local, como en proyectos multilingües internacionales.',
          text: '',
          link:
            {
              text: 'VER DETALLE',
              url: '/servicios/seo/acompanamiento-seo/seo-local-y-seo-internacional',
            },
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-1_2.png',
          title: 'Análisis de competidores',
          subtitle: 'Analizamos a tu competencia tomando en cuenta los más de 200 factores que evalúan los motores de búsqueda. Identificamos oportunidades y te asesoramos consolidando una estrategia de contenidos y un set de palabras claves que te ayudarán a posicionarte en tu industria.',
          text: '',
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
