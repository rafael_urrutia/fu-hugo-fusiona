---
title: SEO Local e Internacional
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'SEO Local e Internacional']
banner:
  {
    type: '1',
    title: ' SEO local e internacional',
    subtitle: 'SEO local geolocalizado  y SEO Internacional multilingüe para ayudar a  nuestros clientes a expandirse y conquistar nuevos mercados internacionales.',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'OPORTUNIDADES',
      text: 'Detectamos oportunidades a través de keywords de alta demanda en tu rubro.',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'ESTRATEGIA HOLÍSTICA',
      text: 'Creamos una estrategia holística de contenido en base a nuestros hallazgos.',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_1.png',
          title: 'SEO local',
          subtitle: 'El SEO local se centra en ofrecer resultados con relevancia geográfica para los buscadores en función de la ubicación del usuario, entregando datos como horarios de apertura, calificaciones de reseñas e incluso imágenes. Estos bits adicionales de información ayudan a los potenciales clientes a tomar decisiones de compra más rápidas y precisas, incidiendo positivamente en la conversión.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_2.png',
          title: 'SEO internacional',
          subtitle: 'Optimizamos sitios web en el extranjero teniendo en cuenta las especificidades de un mercado local determinado. Tomamos en consideración el idioma, el motor de búsqueda más utilizado, las características culturales del país, así como la idiosincrasia del mercado, todos factores que pueden influir en las decisiones de compra de los usuarios.',
          text: '',
        },
      ],
  }
proyectos1:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: []
js_src: []
---
