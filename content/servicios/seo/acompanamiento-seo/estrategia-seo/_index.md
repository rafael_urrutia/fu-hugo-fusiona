---
title: Estrategia SEO
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Estrategia SEO']
banner:
  {
    type: '1',
    title: 'Estrategia SEO',
    subtitle: 'Recomendaciones estratégicas y técnicas que te permiten alcanzar el mejor posicionamiento en buscadores.',
  }
blockServices1:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'PERSONALIZADO',
      text: 'Personalizado según las necesidades específicas de tu empresa',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: '100% REMOTO',
      text: '100% remoto adaptable a diferentes modalidades de trabajo',
    },
    {
      image: '/img/components/block-services/ico-design.svg',
      title: 'COLABORATIVO',
      text: 'Colaborativo, tu compromiso en este proceso es fundamental para el éxito del proyecto',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_1.png',
          title: 'Expansión de contenido',
          subtitle: 'Las constantes actualizaciones de algoritmos de Google afectan negativamente a los sitios web que tienen escaso contenido o información de pobre calidad. Podemos ayudarte a mejorar el ranking de tu web guiándote en la expansión y mejora del contenido,  asegurándonos de que cumpla con los estándares de calidad de Google.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_2.png',
          title: 'Manejo de Google Rich Snippet',
          subtitle: 'Los rich snippets (o fragmentos enriquecidos) de Google son piezas de información complementaria que aparece al rankear en la primera página de resultados del motor de búsqueda (SERP). Te ayudamos a crear estos fragmentos procurando que el contenido responda en un lenguaje natural a las consultas más comunes de los usuarios.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_3.png',
          title: 'Reglas automatizadas',
          subtitle: 'Con miles de productos en tu base de datos, probablemente puede llevar demasiado tiempo añadir cierta información manualmente para que sea leída por las “arañas” de los buscadores. Aquí es donde los sistemas de gestión de contenido (CMS) o herramientas como Google Tag Manager son útiles. Podemos ayudarte a establecer reglas para datos estructurados, que te permiten crear entradas masivas de datos grupales a tu web, garantizando que sean leídas correctamente por los buscadores.',
          text: '',
        },
      ],
  }
proyectos1:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: []
js_src: []
---
