---
title: Keyword Research
html_src: templates/servicios-level2.html
breadcrumbs: ['Inicio', 'Servicios', 'SEO', 'Keyword Research']
banner:
  {
    type: '1',
    title: 'Keyword Research',
    subtitle: 'Investigamos cuáles son las palabras clave que serán más efectivas para mejorar el posicionamiento de conceptos clave para tu negocio.',
  }
blockServices:
  [
    {
      image: '/img/components/block-services/ico-remote.svg',
      title: 'OPORTUNIDADES',
      text: 'Detectamos oportunidades a través de keywords de alta demanda en tu rubro.',
    },
    {
      image: '/img/components/block-services/ico-lab.svg',
      title: 'ESTRATEGIA HOLÍSTICA',
      text: 'Creamos una estrategia holística de contenido en base a nuestros hallazgos.',
    },
  ]
servicios:
  {
    title: '¿De qué se compone?',
    cards:
      [
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_3.png',
          title: 'Recolección y análisis de palabras clave',
          subtitle: 'Recopilamos entre mil y seis mil palabras clave de distintas fuentes. Utilizamos herramientas de terceros, datos de la industria, información de intención de búsqueda (search intent), contenido SERP,  así como datos propios extraídos de Google Search Console y Google Analytics. Finalmente utilizamos nuestra propia fórmula para descubrir los términos y frases más frecuentes en tu lista de palabras clave.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_2.png',
          title: 'Agrupación de palabras clave',
          subtitle: 'La lista completa de palabras clave descubiertas se divide en grupos separados y específicos. A través de este proceso podemos establecer de manera efectiva las bases de una estrategia de contenido óptima y continua en el tiempo.',
          text: '',
        },
        {
          type: '2',
          image: '/img/templates/servicios/commons/img-services-3_1.png',
          title: 'Mapeo de palabras clave',
          subtitle: 'Finalmente se determina qué páginas deben correlacionarse con qué palabras clave. Identificamos ganancias rápidas y oportunidades para cumplir con los objetivos iniciales y también definimos si es necesario crear nuevas páginas para grupos de palabras clave que aún no están asignadas. El trabajo concluye con una estrategia exhaustiva y holística que integra todos los esfuerzos de SEO y de marketing digital para el posicionamiento.',
          text: '',
        },
      ],
  }
proyectos1:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
css_src: []
js_src: []
---
