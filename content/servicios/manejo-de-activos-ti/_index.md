---
title: Manejo de Activos TI
html_src: templates/servicios-level1.html
breadcrumbs: ['Inicio', 'Servicios', 'Manejo de Activos TI']
banner:
  {
    type: '1',
    title: 'Manejo de Activos TI',
    titleClass: 'u-text-center',
    subtitle: 'Soporte personalizado que permite optimizar, respaldar y actualizar tus plataformas digitales, para que siempre estén disponibles cuando tú o tus clientes las necesiten.',
    subtitleClass: 'u-text-center',
  }
servicios:
  [
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-7_1.png',
      title: 'MONITOREO Y ALERTAS',
      subtitle: 'Realizamos un monitoreo permanente de tus plataformas digitales, alertando sobre potenciales caídas o amenazas con un tiempo de respuesta menor a los cinco minutos. Contamos con un sistema de escalamiento de eventos, con una mesa de ayuda, teléfonos de emergencia y un IVR para cada cliente para asegurar que cualquier eventualidad sea resuelta oportunamente por nuestro equipo. Ofrecemos distintas modalidades de atención: 24x7, 10x5 o 12x6.',
      text: '',
    },
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-7_2.png',
      title: 'RESPALDOS Y REVISIONES DE SEGURIDAD',
      subtitle: 'Realizamos respaldos y revisiones de seguridad periódicos para evaluar la salud de los servicios, evaluando factores como tiempo de respuesta, ping o HTTP status. También trabajamos para mantener los servicios personalizados de acuerdo a los requerimientos de nuestros clientes.',
      text: '',
    },
    {
      type: '2',
      image: '/img/templates/servicios/commons/img-services-7_3.png',
      title: 'ARQUITECTURA DE INFRAESTRUCTURA',
      subtitle: 'Nos adaptamos a cualquier tecnología o infraestructura. También podemos dimensionar, preparar y proponer una arquitectura de infraestructura que se acomode a las necesidades y características propias de cada cliente. Trabajamos con servicios cloud como Amazon Web Services y Azure.',
      text: '',
    },
  ]
css_src: []
js_src: []
---
