---
title: Nosotros
html_src: pages/nosotros.html
breadcrumbs: ['Inicio', 'Nosotros']
banner:
  {
    type: '5',
    bannerClass: 'u-color-green2 u-overflow c-banner__padding-b',
    image: '/img/pages/nosotros/banner.png',
    title: 'nosotros',
    titleClass: 'u-text-center',
    subtitle: 'Nos gusta la tecnología que hace la vida<br> más fácil a las personas. <hr>Disfrutamos haciendo nuestro trabajo y eso lo notamos nosotros, nuestras familias y los clientes.',
    text: '',
  }
trayectoria:
  {
    image: '/img/pages/nosotros/img-trayectoria.png',
    text: '13 años de trayectoria y más de cien proyectos para las mejores marcas de Chile convierten a Fusiona en el partner digital que estás buscando.',
  }
experimentado:
  {
    image: '/img/pages/nosotros/img-experimentado.svg',
    text: 'Un experimentado equipo de ingenieros, diseñadores, analistas y expertos en comunicación ayudan a nuestros clientes a cumplir sus objetivos de negocios y sobresalir en el competitivo panorama digital actual.',
  }
pasion:
  {
    image: '/img/pages/nosotros/img-pasion.png',
    text: 'La pasión por lo que hacemos es un reflejo de nuestra cultura organizacional. Un gran clima laboral, beneficios para nuestros colaboradores, oportunidades de crecimiento y un sano equilibrio entre vida laboral y personal hacen de Fusiona un lugar excepcional para trabajar.',
  }
impulsamos:
  {
    title: 'Impulsamos la transformación con proyectos cortos y de <br>alto impacto, insertos en una estrategia integral',
    image: '/img/pages/nosotros/img-impulsamos.png',
    text:
      [
        'Combinamos el diseño de experiencias, la tecnología y el uso de datos para crear estrategias digitales que dan resultado.',
        '13 años de trayectoria y más de cien proyectos para las mejores marcas de Chile convierten a Fusiona en el partner digital que estás buscando.',
      ],
  }
hacemos:
  {
    title: 'Hacemos más eficientes a las organizaciones en sus <br>procesos internos y de cara al cliente',
    boxes:
      [
        {
          boxClass: 'u-color-green1',
          title: 'Confianza',
          text:
            [
              'Desarrollamos soluciones tecnológicas a tu medida, respondiendo a cualquier necesidad digital de tu empresa.',
            ],
        },
        {
          boxClass: 'u-color-orange1',
          title: 'Cálidad humana',
          text:
            [
              'Desarrollamos soluciones tecnológicas a tu medida, respondiendo a cualquier necesidad digital de tu empresa.',
            ],
        },
        {
          boxClass: 'u-color-purple3',
          title: 'innovación',
          text:
            [
              'Desarrollamos soluciones tecnológicas a tu medida, respondiendo a cualquier necesidad digital de tu empresa.',
            ],
        },
        {
          boxClass: 'u-color-blue1',
          title: 'trayectoria',
          text:
            [
              'Desarrollamos soluciones tecnológicas a tu medida, respondiendo a cualquier necesidad digital de tu empresa.',
            ],
        },
      ],
    subtitle: 'LO QUE NOS MUEVE',
    text:
      [
        'Un experimentado equipo de ingenieros, diseñadores, analistas y expertos en comunicación ayudan a nuestros clientes a cumplir sus objetivos de negocios y sobresalir en el competitivo panorama digital actual.',
        'La pasión por lo que hacemos es un reflejo de nuestra cultura organizacional. Un gran clima laboral, beneficios para nuestros colaboradores, oportunidades de crecimiento y un sano equilibrio entre vida laboral y personal hacen de Fusiona un lugar excepcional para trabajar.',
      ],
  }
full:
  {
    title: 'FULL DESARROLLO',
    text: 'Validación de plataformas y prototipos con usuarios reales, de manera remota. Se genera un <br>informe con hallazgos y sugerencias como entregable.',
    boxes:
      [
        {
          value: '150',
          addPlus: true,
          titleClass: 'u-text-color-orange2',
          title: 'desarrollos web',
          text: 'Este servicio se entrega <br>eficazmente en el contexto de <br>aislamiento social',
        },
        {
          value: '20',
          addPlus: true,
          titleClass: 'u-text-color-orange3',
          title: 'apps',
          text: 'Contamos con infraestructura <br>para el desarrollo de focus <br>y pruebas con usuarios',
        },
        {
          value: '40000',
          addPlus: false,
          titleClass: 'u-text-color-purple4',
          title: 'horas de mantención',
          text: 'las soluciones finales son <br>pensadas para ser <br>escalables',
        },
        {
          value: '120',
          addPlus: true,
          titleClass: 'u-text-color-light-blue',
          title: 'campañas de mktg digital',
          text: 'las soluciones finales son <br>pensadas para ser <br>escalables',
        },
      ],
  }
equipo: { title: 'Conoce a nuestro equipo' }
beneficios:
  {
    title: 'BENEFICIOS',
    boxes:
      [
        {
          icon: '/img/pages/nosotros/ico-beneficios-1.svg',
          title: 'Seguro complementario',
          text: '50% cofinanciado por Fusiona  ',
        },
        {
          icon: '/img/pages/nosotros/ico-beneficios-3.svg',
          title: 'Tarde libre',
          text: 'el día de tu cumpleaños y el de tus hijos',
        },
        {
          icon: '/img/pages/nosotros/ico-beneficios-4.svg',
          title: 'Jornada reducida',
          text: 'salida a las 18:30 de Lunes a Jueves, los viernes 18:00',
        },
      ],
  }
postula:
  {
    title: 'POSTULA',
    text: 'En Fusiona estamos constantemente buscando gente apasionada por lo que hace y capaz de aportar a nuestro equipo. Si te interesa ser parte de nuestra empresa, revisa los puestos disponibles y escríbenos.',
  }
ofertas:
  {
    image: '/img/pages/nosotros/img-ofertas.png',
    title: 'SUSCRÍBETE A NUESTRAS OFERTAS',
    text: 'Puedes suscribirte a nuestras ofertas laborales directamente a través de nuestras cuentas en Linkedin o Getonboard.',
    linkLinkedin:
      { url: 'https://cl.linkedin.com/company/fusiona', text: 'linkedin' },
    linkGetonboard:
      {
        url: 'https://www.getonbrd.com/companies/fusiona',
        text: 'get on board',
      },
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
tabs:
  {
    type: '1',
    tabs:
      [
        { name: 'Equipo', id: 'team' },
        { name: 'TRABAJA CON NOSOTROS', id: 'work' },
      ],
  }

css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
