---
title: Fusiona
baseURL: '/'
languageCode: 'es'
banner:
  {
    type: '2',
    link: { text: '¿listo para iniciar tu proyecto?', url: '/contacto' },
    slider:
      [
        {
          wrapperClass: 'p-home__banner-wrapper--default',
          classColorText: 'u-text-color-white',
          imageBackground: '/img/pages/home/banner/banner-background-2.png',
          case: 'SERVICIOS',
          titleClass: 'p-home__banner-title--default',
          title: 'Los números nos inspiran, <br>la tecnología nos conduce',
          subtitleClass: 'p-home__banner-subtitle--default',
          subtitle: 'Descubre nuestros servicios y cómo pueden, <br>ayudarte a alcanzar tus objetivos de negocio.',
          link: { text: 'VER DETALLE', url: '/servicios/' },
          imageClass: 'p-home__banner-image--default',
          image: '/img/pages/home/banner/image-2.png',
          imageMobile: '/img/pages/home/banner/image-2.png',
        },
        {
          wrapperClass: 'p-home__banner-wrapper--default',
          classBackground: 'u-color-orange4',
          classColorText: 'u-text-color-gray6',
          case: 'SERVICIO UX',
          titleClass: 'p-home__banner-title--default',
          title: 'Experiencias excepcionales, <br>que dan resultados <br>excepcionales',
          subtitleClass: 'p-home__banner-subtitle--default',
          subtitle: 'Conoce más del trabajo de<br>nuestro equipo de UX.',
          link:
            { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
          imageClass: 'p-home__banner-image--floatBottomRight',
          image: '/img/pages/home/banner/image-3.png',
          imageMobile: '/img/pages/home/banner/image-3-mobile.png',
        },
        {
          wrapperClass: 'p-home__banner-wrapper--default',
          classBackground: 'u-color-purple3',
          classColorText: 'u-text-color-white',
          case: 'PROYECTOS',
          titleClass: 'p-home__banner-title--default',
          title: 'Reunimos lo mejor de dos <br>mundos: consultoría estratégica <br>digital y fábrica de software',
          subtitleClass: 'p-home__banner-subtitle--default',
          subtitle: 'Analistas e ingenieros expertos<br>construyen proyectos con sentido<br>y sólidas bases técnicas.',
          link: { text: 'VER DETALLE', url: '/proyectos' },
          imageClass: 'p-home__banner-image--floatBottomRight',
          image: '/img/pages/home/banner/image-4.png',
          imageMobile: '/img/pages/home/banner/image-4-mobile.png',
        },
      ],
  }
servicios:
  {
    title: 'SERVICIOS',
    subtitle: 'Soluciones digitales que responden a tus objetivos de negocio',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-experiencia.png',
          title: 'EXPERIENCIA DE USUARIO',
          subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
          link:
            { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-analitica.png',
          title: 'ANALÍTICA DIGITAL',
          subtitle: 'Transformamos tus datos en insights y te ayudamos a desarrollar una estrategia de análisis digital personalizada para tu negocio.',
          link: { text: 'VER DETALLE', url: '/servicios/analitica-digital/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-design.svg',
          title: 'DISEÑO DIGITAL',
          subtitle: 'Proponemos diseños que reflejan tu identidad e imagen de marca de manera atractiva y moderna de cara a los usuarios.',
          link: { text: 'VER DETALLE', url: '/servicios/diseno-digital/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-seo.png',
          title: 'SEO',
          subtitle: 'Mejoramos los resultados orgánicos en buscadores con una estrategia SEO diseñada específicamente para tu negocio.',
          link: { text: 'VER DETALLE', url: '/servicios/seo/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link:
            { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-mantencion.png',
          title: 'MANTENCIÓN',
          subtitle: 'Nuestro equipo de expertos trabaja para mantener y hacer crecer tus plataformas digitales, acercarte a tus clientes y mejorar tus conversiones.',
          link: { text: 'VER DETALLE', url: '/servicios/mantencion/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-soporte.png',
          title: 'MANEJO DE ACTIVOS TI',
          subtitle: 'Soporte personalizado que permite optimizar, respaldar y actualizar tus plataformas digitales, para que siempre estén disponibles cuando tú o tus clientes las necesiten.',
          link:
            { text: 'VER DETALLE', url: '/servicios/manejo-de-activos-ti/' },
        },
      ],
  }
proyectos:
  {
    title: 'PROYECTOS',
    subtitle: 'Revisa nuestro portafolio con los trabajos que hemos realizado para nuestros clientes',
    slider:
      [
        {
          classSlide: 'u-color-gray5',
          case: 'GASCO',
          title: 'Gasconnect:<br> la app de pedido de gas que cambió la industria',
          subtitle: '',
          link: { text: 'VER DETALLE', url: '/proyectos/gasconnet/' },
          imageClass: 'p-home__slider-image--projects-bottom',
          imageDesktop: '/img/pages/home/proyectos/slider-gasco.png',
          imageMobile: '/img/pages/home/proyectos/slider-kauffmann-mobile.png',
        },
        {
          classSlide: 'u-color-green1',
          case: 'CLÍNICA SANTA MARÍA',
          title: 'Creando una experiencia digital de excelencia dentro de la industria de la salud en Chile',
          subtitle: '',
          link: { text: 'VER DETALLE', url: '/proyectos/clinica-santa-maria/' },
          imageClass: 'p-home__slider-image--projects-bottom',
          imageDesktop: '/img/pages/home/proyectos/slider-CSM.png',
          imageMobile: '/img/pages/home/proyectos/slider-kauffmann-mobile.png',
        },
        {
          classSlide: 'u-color-gray5',
          case: 'MOVISTAR',
          title: 'Acompañando a Movistar hacia la madurez digital',
          subtitle: '',
          link: { text: 'VER DETALLE', url: '/proyectos/movistar/' },
          imageClass: 'p-home__slider-image--projects-bottom',
          imageDesktop: '/img/pages/home/proyectos/slider-movistar.png',
          imageMobile: '/img/pages/home/proyectos/slider-kauffmann-mobile.png',
        },
        {
          classSlide: 'u-color-green1',
          case: 'BANCO DE CHILE',
          title: 'Simplificando los pasos para solicitar un crédito online',
          subtitle: '',
          link:
            { text: 'VER DETALLE', url: '/proyectos/banco-de-chile-credito/' },
          imageClass: 'p-home__slider-image--projects-bottom',
          imageDesktop: '/img/pages/home/proyectos/slider-BCH.png',
          imageMobile: '/img/pages/home/proyectos/slider-kauffmann-mobile.png',
        },
      ],
  }
clientes:
  {
    title: 'CLIENTES',
    subtitle: 'Importantes marcas nos han confiado sus proyectos digitales',
    clientes:
      [
        { image: '/img/pages/home/clientes/logo_australis.png', url: '' },
        { image: '/img/pages/home/clientes/logo_bch.png', url: '' },
        { image: '/img/pages/home/clientes/logo_credichile.png', url: '' },
        { image: '/img/pages/home/clientes/logo_buinzoo.png', url: '' },
        { image: '/img/pages/home/clientes/logo_CSM.png', url: '' },
        { image: '/img/pages/home/clientes/logo_banmedica.png', url: '' },
        { image: '/img/pages/home/clientes/logo_gasco.png', url: '' },
        { image: '/img/pages/home/clientes/logo_inmobilia.png', url: '' },
        { image: '/img/pages/home/clientes/logo_movistar.png', url: '' },
        { image: '/img/pages/home/clientes/logo_telefonica.png', url: '' },
        { image: '/img/pages/home/clientes/logo_nuevopudahuel.png', url: '' },
        { image: '/img/pages/home/clientes/logo_travel.png', url: '' },
        { image: '/img/pages/home/clientes/logo_watts.png', url: '' },
        { image: '/img/pages/home/clientes/logo_forum.png', url: '' },
        { image: '/img/pages/home/clientes/logo_gac.png', url: '' },
        { image: '/img/pages/home/clientes/logo_yocreo.png', url: '' },
        { image: '/img/pages/home/clientes/logo_medioambiente.png', url: '' },
        { image: '/img/pages/home/clientes/logo_USS.png', url: '' },
      ],
  }
nosotros:
  {
    title: 'NOSOTROS',
    subtitle: 'Somos una de las empresas digitales líderes en el país, con un equipo multidisciplinario de profesionales expertos y un excelente clima laboral.',
    linkMeet: { text: 'CONÓCENOS', url: '/nosotros' },
    linkWork: { text: 'TRABAJA CON NOSOTROS', url: '/nosotros#work-to-us' },
  }

css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
