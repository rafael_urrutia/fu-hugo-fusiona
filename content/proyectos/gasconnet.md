---
title: Gasco
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Gasco']
logo: '/img/pages/proyectos/logos/P_gasco_logo@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/GASCO/gasco-banner.png',
    bannerClass: 'u-color-gray5',
    minititle: 'Gasco',
    title: 'Gasconnect:<br/> la app de pedido de gas que cambió la industria ',
    textClass: 'c-banner__text--custom',
    text: '',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Gasconnect proporcionó un cambio tecnológico que simplificó la vida de los usuarios y, de paso, transformó el negocio de Gasco”',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text: 'Luego de cinco años de trabajo ininterrumpido, Gasconnect se ha convertido en un pilar de la estrategia de ventas de Gasco. Fue una de las primeras aplicaciones de pedidos de cilindros de gas a domicilio en Chile y su éxito se ve reflejado en las miles de compras que se generan desde este canal todos los años. Pero, para seguir haciendo crecer esta plataforma, ha sido necesario realizar diversos proyectos que apuntan a reforzar su infraestructura tecnológica.',
        },
        {
          title: 'Proceso',
          text:
            'Fusiona realiza mensualmente diversas tareas de mantención tecnológica y de soporte de la app, tanto de su versión para consumidores como la que utilizan los conductores de los camiones repartidores. Dicha mantención involucra desde mejoras en el código, hasta la integración con otros servicios externos fundamentales para el negocio de Gasco. También se han desarrollado módulos satélite, como un sistema de trackeo para los despachadores que se encuentran en las oficinas de Gasco y que incluye un semáforo con un umbral de tiempo de atención de todos los pedidos. <br /><br />
            Sin embargo, para asegurar un crecimiento sostenido de la plataforma, se ha hecho necesario repensar toda la estructura que soporta la app y transitar a una arquitectura basada en microservicios. Esta nueva arquitectura permite acoplar un nuevo algoritmo de asignación de pedidos diseñado por un proveedor externo y sentar bases sólidas para la futura incorporación de nuevas funcionalidades.<br /><br />',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/GASCO/gasco-img.png'],
        legend: 'Gasconnect fue uno de los primeros aplicativos en Chile que permitió hacer pedidos de cilindros de gas al hogar desde dispositivos móviles.',
      },
    down_content:
      [
        {
          title: 'Resultados',
          text: 'Desde que se formó la alianza Gasco - Fusiona, el uso de esta plataforma ha aumentado exponencialmente. Hoy ya se cuentan más de 500 mil descargas de la app en tiendas y los pedidos a través de la app superan con creces los 100 mil todos los años.',
        },
      ],
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/GASCO/gasco-gallery-img-01.png',
      legend: 'Además de pedir los cilindros, la app permite al usuario hacer seguimiento de la ubicación del camión repartidor en el camino a su hogar.',
    },
    {
      image: '/img/templates/proyecto/GASCO/gasco-gallery-img-02.png',
      legend: 'Una vez hecho el pedido la app muestra un resumen de la solicitud, incluyendo su valor y tiempo aproximado de entrega.',
    },
    {
      image: '/img/templates/proyecto/GASCO/gasco-gallery-img-03.png',
      legend: 'Para un registro más rápido, el usuario puede acceder a la app usando su cuenta de Facebook.',
    },
    {
      image: '/img/templates/proyecto/GASCO/gasco-gallery-img-04.png',
      legend: 'El usuario registrado puede ver un historial de sus compras en la app.',
    },
    {
      image: '/img/templates/proyecto/GASCO/gasco-gallery-img-05.png',
      legend: 'Gasconnect cuenta con una versión de la app para los conductores de los camiones repartidores, la cual permite tomar pedidos y establecer la ruta más rápida de entrega.',
    },
    {
      image: '/img/templates/proyecto/GASCO/gasco-gallery-img-06.png',
      legend: 'Al igual que la app de consumidores, la versión para los conductores ofrece la opción de ver el historial de entregas realizadas.',
    },
  ]
servicios:
  {
    servicesClass: 'title-center',
    title: 'Servicios utilizados',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link:
            { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
