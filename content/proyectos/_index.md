---
title: Proyectos
html_src: pages/proyectos.html
breadcrumbs: ['Inicio', 'Proyectos']
banner:
  {
    type: '1',
    title: 'Proyectos',
    titleClass: 'u-text-center',
    subtitle: 'Te invitamos a revisar parte del portafolio que<br> refleja nuestra trayectoria y experiencia.',
    subtitleClass: 'u-text-center',
  }
tabs:
  {
    type: '1',
    tabs:
      [
        { name: 'Sitio Web', id: 'web_site' },
        { name: 'Apps', id: 'apps' },
        { name: 'Desarrollo', id: 'development' },
      ],
  }

web_content:
  [
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card c-cards__projects--half-width',
      cardClass: 'u-color-gray5',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_movistar.png',
      title: 'movistar',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Acompañando a Movistar hacia la madurez digital',
      linkButton: { text: 'VER DETALLE', url: '/proyectos/movistar' },
    },
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card',
      cardClass: 'u-color-purple3',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_BCH1.png',
      title: 'BANCO DE CHILE',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Construyendo la mejor app de beneficios de la industria bancaria',
      linkButton: { text: 'VER DETALLE', url: '/proyectos/banco-de-chile-app' },
    },
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card',
      cardClass: 'u-color-gray5',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_gasconnect.png',
      title: 'gasco',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
      linkButton: { text: 'VER DETALLE', url: '/proyectos/gasconnet' },
    },
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card',
      cardClass: 'u-color-green1',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_CSM.png',
      title: 'Clínica Santa María',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Creando una experiencia digital de excelencia dentro de la industria de la salud en Chile',
      linkButton:
        { text: 'VER DETALLE', url: '/proyectos/clinica-santa-maria' },
    },
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card',
      cardClass: 'u-color-gray5',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_NPudahuel.png',
      title: 'Nuevo Pudahuel',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Una mantención realmente evolutiva para el aeropuerto más importante de Chile',
      linkButton: { text: 'VER DETALLE', url: '/proyectos/nuevo-pudahuel' },
    },
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card',
      cardClass: 'u-color-orange1',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_forum.png',
      title: 'forum',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Una nueva forma de comprar tu automóvil',
      linkButton: { text: 'VER DETALLE', url: '/proyectos/forum' },
    },
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card  c-cards__projects--half-width',
      cardClass: 'u-color-green1',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_BCH2.png',
      title: 'banco de chile',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Simplificando los pasos para solicitar un crédito online',
      linkButton:
        { text: 'VER DETALLE', url: '/proyectos/banco-de-chile-credito' },
    },
    {
      type: '3',
      heightClass: 'c-cards__projects--normal-card',
      cardClass: 'u-color-orange1',
      imageClass: 'c-cards__projects-image--normal-card',
      image: '/img/pages/proyectos/thum_ENEX.png',
      title: 'ENEX',
      subtitleClass: 'c-cards__projects-subtitle--normal-card',
      subtitle: 'Segmentando beneficios para la App Mi Copiloto',
      linkButton: { text: 'VER DETALLE', url: '/proyectos/enex' },
    },
  ]
app_content: []
develop_content: []
servicios:
  {
    title: 'SERVICIOS',
    subtitle: 'Nuestros servicios entregan soluciones digitales concretas acorde a los objetivos de negocio',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-experiencia.png',
          title: 'EXPERIENCIA DE USUARIO',
          subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
          link:
            { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-analitica.png',
          title: 'ANALÍTICA DIGITAL',
          subtitle: 'Transformamos tus datos en insights y te ayudamos a desarrollar una estrategia de análisis digital personalizada para tu negocio.',
          link: { text: 'VER DETALLE', url: '/servicios/analitica-digital/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-design.svg',
          title: 'DISEÑO DIGITAL',
          subtitle: 'Proponemos diseños que reflejan tu identidad e imagen de marca de manera atractiva y moderna de cara a los usuarios.',
          link: { text: 'VER DETALLE', url: '/servicios/diseno-digital/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-seo.png',
          title: 'SEO',
          subtitle: 'Mejoramos los resultados orgánicos en buscadores con una estrategia SEO diseñada específicamente para tu negocio.',
          link: { text: 'VER DETALLE', url: '/servicios/seo/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link:
            { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-mantencion.png',
          title: 'MANTENCIÓN',
          subtitle: 'Nuestro equipo de expertos trabaja para mantener y hacer crecer tus plataformas digitales, acercarte a tus clientes y mejorar tus conversiones.',
          link: { text: 'VER DETALLE', url: '/servicios/mantencion/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-soporte.png',
          title: 'MANEJO DE ACTIVOS TI',
          subtitle: 'Soporte personalizado que permite optimizar, respaldar y actualizar tus plataformas digitales, para que siempre estén disponibles cuando tú o tus clientes las necesiten.',
          link:
            { text: 'VER DETALLE', url: '/servicios/manejo-de-activos-ti/' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
