---
title: Banco de Chile App Beneficios
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Banco de Chile App Beneficios']
logo: '/img/pages/proyectos/logos/P_bancochile_logo@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/BCH-APP/BCH_APP-banner.png',
    bannerClass: 'u-color-purple3',
    minititle: 'Banco de Chile',
    title: 'Construyendo la mejor app de beneficios de la industria bancaria',
    textClass: 'c-banner__text--custom',
    text: '',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Aplicamos UX y tecnología para crear una aplicación útil y fácil de usar para los clientes del banco”',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text: 'Banco de Chile buscaba crear una app de fidelización separada de la aplicación transaccional del Banco y, adicionalmente, se pretendía crear un sistema de autoadministración que permitiera actualizar dicha plataforma diariamente.',
        },
        {
          title: 'Proceso',
          text:
            'El proyecto comenzó con una investigación para identificar las necesidades de los clientes en lo relacionado al uso de los beneficios del Banco, además de un benchmark de la industria de aplicaciones bancarias. Este levantamiento permitió establecer perfiles de usuarios y posteriormente definir una arquitectura y diseño acorde a esos perfiles. Una vez concluído el desarrollo y la publicación de la app, Fusiona creó un sistema de administración que permitía sumar o modificar los beneficios rápidamente.<br /><br />
            En paralelo, se establecieron las bases para la mantención evolutiva de la app y el sistema de administración. Como parte de este proceso de acompañamiento se creó un equipo encargado de mantener el contenido, actualizar las campañas y responder a las preguntas de los usuarios. Además, para garantizar que las calificaciones de las app se mantuvieran altas, se realizaron periódicamente investigaciones y entrevistas con clientes para detectar problemas y buscar soluciones.<br /><br />
            Durante ocho años la app ha vivido distintos procesos de mejora continua, con optimizaciones al diseño en base a los mencionados estudios de experiencia de usuario, además de realizar distintos upgrades al sistema de administración de contenidos.',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/BCH-APP/BCH_APP-img.png'],
        legend: 'La app de beneficios de Banco de Chile se diseñó para fidelizar a distintos segmentos de clientes según sus intereses.',
      },
    down_content: [{ title: 'Resultado', text: '<ul>
            <li>La App Mi Beneficio fue pionera en la prueba de tecnologías, funcionalidades y campañas, creando sólidas bases de experimentación y conocimiento para Banco de Chile en sus plataformas móviles. Así pasó, por ejemplo, con iniciativas vinculadas al uso de mensajería push y la implementación de beacons en tiendas.</li>
            <li>Estas experiencias, basadas en el uso de data y la investigación permanente, crearon un entorno de mejora constante y aprendizaje mutuo entre la célula Fusiona y el Banco.</li>
            <li>La app se posicionó rápidamente en el mercado, con cerca de 70 mil usuarios recurrentes que utilizaban códigos de descuento y aprovechaban las ofertas disponibles.</li>
            <li><em>Mi Beneficio</em> se convirtió en la tercera aplicación más utilizada del Banco, luego de la aplicación transaccional <em>(Mi Banco)</em> y la de autentificación <em>(Mi Pass)</em>.</li>
            </ul>' }],
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/BCH-APP/BCH_APP-gallery-img-01.png',
      legend: 'La aplicación permite a los clientes “canjear” sus puntos acumulados como parte del programa de fidelización del banco.',
    },
    {
      image: '/img/templates/proyecto/BCH-APP/BCH_APP-gallery-img-02.png',
      legend: 'La app además entrega información respecto a las condiciones de los beneficios, las cuales son actualizadas por el equipo Fusiona. ',
    },
    {
      image: '/img/templates/proyecto/BCH-APP/BCH_APP-gallery-img-03.png',
      legend: 'El usuario puede navegar en la app de acuerdo al tipo de beneficio que ofrece el banco. ',
    },
  ]
servicios:
  {
    servicesClass: 'title-center',
    title: 'Servicios utilizados',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-experiencia.png',
          title: 'EXPERIENCIA DE USUARIO',
          subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
          link: { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-design.svg',
          title: 'DISEÑO DIGITAL',
          subtitle: 'Proponemos diseños que reflejan tu identidad e imagen de marca de manera atractiva y moderna de cara a los usuarios.',
          link: { text: 'VER DETALLE', url: '/servicios/diseno-digital/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link: { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
