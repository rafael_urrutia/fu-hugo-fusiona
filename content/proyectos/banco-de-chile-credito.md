---
title: Banco de Chile
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Banco de Chile crédito']
logo: '/img/pages/proyectos/logos/P_bancochile_logo@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/BCH-C/BCH_C-banner.png',
    bannerClass: 'u-color-green1',
    minititle: 'Banco de Chile',
    title: 'Simplificando los pasos para solicitar un crédito online',
    textClass: 'c-banner__text--custom',
    text: '',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Desarrollamos una estrategia que permitió agilizar el enrolamiento online y capturar más clientes a través del canal online”',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text: 'Banco de Chile, una de las instituciones financieras líderes del país, buscaba implementar un sistema que permitiera evaluar en tiempo real a quienes solicitaran un crédito por medio de su página web. Para ello se debía identificar con certeza al usuario, obtener información sobre su historial financiero y viabilidad de acceso a crédito y finalmente agendar una visita a la sucursal física.',
        },
        {
          title: 'Proceso',
          text:
            'El proyecto desarrollado por Fusiona incluyó un completo cambio en la tecnología en que estaba construido el flujo de autoatención, privilegiando sobre todo el componente de seguridad de la plataforma. Además se realizó un cambio en la infraestructura, pasando de un data center físico a uno en la nube.<br /><br />
            Por otro lado, se creó todo un proceso de autentificación del solicitante al integrarse con bases de datos del Registro Civil y Previred, servicio que permite el pago de cotizaciones previsionales en Chile. Una vez identificado el usuario, el sistema realiza una serie de consultas para identificar el perfil financiero del potencial cliente y su historial de consumo de productos bancarios, para luego concluir el proceso con un agendamiento en la sucursal más cercana para realizar la firma presencial. <br /><br />
            Adicionalmente el proyecto incluyó varios cambios de diseño UI en el flujo de solicitud de crédito para mejorar la experiencia del usuario y el look & feel de la página, además de la creación de un back office con información valiosa que permite tomar decisiones a ejecutivos y analistas del Banco y que incluye información como registros llenados, puntos de abandono y bloqueos en caso de amenaza de seguridad.',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/BCH-C/BCH_C-img.png'],
        legend: 'En sólo unos cuantos pasos un usuario promedio puede pedir un crédito online del Banco de Chile.',
      },
    down_content:
      [
        {
          title: 'Resultados',
          text: 'La iniciativa trajo consigo un fuerte impacto en el trabajo de ejecutivos de canales remotos y presenciales, al disminuir el tiempo que invierten en la búsqueda y análisis de la información de clientes, la que hoy obtienen de forma automatizada. Además, gracias a la simpleza del flujo creado, hoy semanalmente se reciben en promedio mil registros semanales de usuarios que realizan el enrolamiento a través de la página web del Banco. Para concluir, el éxito de este proyecto condujo a que se replicara en todas las marcas del Banco, implementándose también en Banco Credichile y Banco Edwards.',
        },
      ],
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/BCH-C/BCH_C-gallery-img-01.png',
      legend: 'El proceso de enrolamiento para la solicitud de crédito puede ser realizado tanto en dispositivos móviles como de escritorio.',
    },
    {
      image: '/img/templates/proyecto/BCH-C/BCH_C-gallery-img-02.png',
      legend: 'Cumplir con estrictas medidas de seguridad y de validación de la identidad del usuario fueron los principales desafíos de este proyecto.',
    },
  ]

servicios:
  {
    servicesClass: 'title-center',
    title: 'Servicios utilizados',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-design.svg',
          title: 'DISEÑO DIGITAL',
          subtitle: 'Proponemos diseños que reflejan tu identidad e imagen de marca de manera atractiva y moderna de cara a los usuarios.',
          link: { text: 'VER DETALLE', url: '/servicios/diseno-digital/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link: { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
