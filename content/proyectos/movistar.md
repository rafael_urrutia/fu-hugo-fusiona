---
title: Movistar
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Movistar']
logo: '/img/pages/proyectos/logos/P_movistar_logo@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/Movistar/movistar-banner.png',
    bannerClass: 'u-color-gray5',
    minititle: 'Movistar',
    title: 'Acompañando a Movistar hacia la madurez digital',
    textClass: 'c-banner__text--custom',
    text: '',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Un servicio integral de acompañamiento para transformar la web de Movistar en uno de los principales canales de venta de la compañía”',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text: 'Durante los 13 años de vida de Fusiona hemos forjado una relación comercial con Movistar  basada en la confianza y la flexibilidad, promoviendo y colaborando en la evolución permanente de sus canales online. La transformación de la web de Movistar desde una plataforma informativa a una página de ecommerce y autogestión, ha requerido profundizar esta alianza a partir del intenso trabajo de distintos equipos compuestos por expertos en tecnología, comunicación digital, métricas y experiencia de usuario.',
        },
        {
          title: 'Proceso',
          text:
            'Un equipo de Fusiona realiza diariamente la mantención de contenidos de la web de Movistar, incluyendo el catálogo de productos y equipos que se pueden comprar a través de esta plataforma. Este proceso va de la mano con un trabajo de expertos en UX, quienes conducen distintos estudios con usuarios y proponen mejoras a las interfaces en base a los preceptos de la usabilidad. Este equipo -que trabaja como célula usando la metodología ágil scrum- además se encarga de actualizar el diseño de las páginas en función de los lineamientos que entrega Movistar España al resto de sus oficinas regionales.<br /><br />
            Adicionalmente, y a partir de diversas fuentes de datos, un equipo de métricas proporciona insumos para la toma de decisiones por parte de las distintas áreas de negocios involucradas en el contenido de la web. Participa, asimismo, en la marcación y seguimiento de las principales conversiones del sitio, así como de las campañas en medios digitales que efectúa la compañía.<br /><br />
            En paralelo a las actividades diarias, Fusiona realiza de forma periódica proyectos de mejora en distintos frentes, lo que incluye sucesivos cambios a la tecnología en que está construída la página principal de Movistar y un asesoramiento continuo respecto a la infraestructura de servicios.<br />
            Otro de los proyectos destacados es el desarrollo de la plataforma Ecare, un servicio de preguntas frecuentes para que los usuarios puedan resolver por sí mismos inquietudes técnicas relacionadas a los productos de la empresa. También se han creado mini sitios y páginas especiales -como las que se utilizan para los eventos Cyber- y que reciben un número importante de visitas concurrentes.',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/Movistar/movistar-img.png'],
        legend: 'Hoy el sitio web de Movistar es una multiplataforma informativa, comercial y de ecommerce.',
      },
    down_content:
      [
        {
          title: 'Resultados',
          text: 'En la actualidad la web de Movistar es una página eminentemente enfocada en el ecommerce y la autoatención, con una actualización permanente de contenidos y un foco claro en la conversión. Gracias al gran trabajo técnico, de análisis y experiencia de usuario realizado por Fusiona, hoy la plataforma web es uno de los principales canales de venta para Movistar.',
        },
      ],
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/Movistar/movistar-gallery-img-01.png',
      legend: 'Los usuarios pueden iniciar el proceso de contratación de productos desde la página web en su versión desktop o móvil.',
    },
    {
      image: '/img/templates/proyecto/Movistar/movistar-gallery-img-02.png',
      legend: 'Fusiona ha ayudado a crear los sitios dedicados a los eventos Cyber en los que Movistar participa.',
    },
    {
      image: '/img/templates/proyecto/Movistar/movistar-gallery-img-03.png',
      legend: 'Métricas digitales y Experiencia de Usuario son dos áreas en las que Fusiona asesora a Movistar como parte de su acompañamiento estratégico.',
    },
    {
      image: '/img/templates/proyecto/Movistar/movistar-gallery-img-04.png',
      legend: 'El sitio web de Movistar ha evolucionado para transformarse progresivamente en una plataforma de autoatención.',
    },
    {
      image: '/img/templates/proyecto/Movistar/movistar-gallery-img-05.png',
      legend: 'Como parte de sus tareas de mantención, Fusiona debe actualizar promociones y contenidos comerciales que provienen de distintas áreas internas de Movistar.',
    },
    {
      image: '/img/templates/proyecto/Movistar/movistar-gallery-img-06.png',
      legend: 'Las guías a usuarios y secciones con preguntas frecuentes también son revisadas y actualizadas por el equipo Fusiona.',
    },
    {
      image: '/img/templates/proyecto/Movistar/movistar-gallery-img-07.png',
      legend: 'Elementos como los CTA son revisados y publicados por el equipo de mantención Movistar de Fusiona.',
    },
  ]
servicios:
  {
    servicesClass: 'title-center',
    title: 'Servicios utilizados',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-experiencia.png',
          title: 'EXPERIENCIA DE USUARIO',
          subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
          link:
            { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-analitica.png',
          title: 'ANALÍTICA DIGITAL',
          subtitle: 'Transformamos tus datos en insights y te ayudamos a desarrollar una estrategia de análisis digital personalizada para tu negocio.',
          link: { text: 'VER DETALLE', url: '/servicios/analitica-digital/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link:
            { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-mantencion.png',
          title: 'MANTENCIÓN',
          subtitle: 'Nuestro equipo de expertos trabaja para mantener y hacer crecer tus plataformas digitales, acercarte a tus clientes y mejorar tus conversiones.',
          link: { text: 'VER DETALLE', url: '/servicios/mantencion/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
