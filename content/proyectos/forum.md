---
title: Forum
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Forum']
logo: '/img/pages/proyectos/logos/P_forum@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/FORUM/forum_banner.png',
    bannerClass: 'u-color-orange1',
    minititle: 'Forum',
    title: 'Forum: una nueva forma de comprar tu automóvil',
    textClass: 'c-banner__text--custom',
    text: '',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Una propuesta innovadora para ayudar a los clientes a pedir el crédito automotriz que necesitan”.',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text: '¿Cómo nos acercamos a los usuarios desde una perspectiva más humana? Ese fue el principal desafío que nos encomendó Forum, empresa que es parte del grupo BBVA y que presta servicios financieros asociados a cientos de concesionarios de automóviles en Chile. Nuestra respuesta fue un proyecto multidisciplinario cuyo objetivo central fue la mejora permanente de las conversiones, específicamente de las cotizaciones en la nueva web.',
        },
        {
          title: 'El proceso',
          text:
            'Ante la necesidad de entregarle a la nueva web de Forum un look & feel alejado de la imagen tradicional que tienen las financieras, el equipo de UX de Fusiona creó un “camino” de navegación que permite al usuario encontrar el vehículo y crédito ideal de acuerdo a su perfil y preferencias de pago. Para ello, se realizó un proceso de investigación y levantamiento de información con usuarios que permitió definir perfiles y sus expectativas respecto a la web.<br /><br />
            En paralelo, desde el punto de vista del desarrollo tecnológico se sortearon una serie de complejidades, entre ellas la integración con distintos servicios REST y el cumplimiento de una serie de estrictos protocolos de seguridad que son parte del funcionamiento interno de Forum.<br /><br />Ya en la etapa de mantención evolutiva del sitio, se continúan haciendo trabajos de mejora permanente de la experiencia de usuario en los flujos más importantes, así como implementando nuevas reglas de negocio derivadas de la contingencia producida por el  Covid-19. Adicionalmente, se realiza un activo acompañamiento para garantizar que las nuevas acciones que se creen en el sitio cumplan con todos los estándares de seguridad propios de Forum.<br /><br />Finalmente, nuestro equipo de Métricas realiza distintas iniciativas para aumentar las conversiones. A partir de la data recolectada desde diversos medios, identifican segmentos de usuarios clave, a quienes se les entregan mensajes particulares según su perfilamiento. De esta forma se logra incrementar el número de cotizaciones que mes a mes se procesan a través de la página web.',
        },
        {
          title: 'Resultado',
          text: 'Hoy el sitio de Forum es una plataforma amigable y que permite al usuario encontrar rápida y fácilmente el vehículo y el crédito que más se acomoda a sus requerimientos. Una plataforma en constante evolución y que está a la punta en el mercado del financiamiento automotriz.',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/FORUM/forum-gallery-img-01.png'],
        legend: 'La nueva web de Forum puso el foco en las necesidades del usuario según su perfil de comprador.',
      },
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/FORUM/forum-gallery-img-01.png',
      legend: 'Forum construyó un sitio pensado para hacer la selección de un crédito automotriz lo más rápido y cómodo posible.',
    },
    {
      image: '/img/templates/proyecto/FORUM/forum-gallery-img-02.png',
      legend: 'La plataforma es completamente responsive para ser leída desde cualquier dispositivo.',
    },
    {
      image: '/img/templates/proyecto/FORUM/forum-gallery-img-03.png',
      legend: 'En un paso a paso guiado, el cliente puede seleccionar el vehículo y el crédito que más le convenga.',
    },
    {
      image: '/img/templates/proyecto/FORUM/forum-gallery-img-04.png',
      legend: 'El usuario puede acceder a distintas formas de contacto con Forum para culminar su compra.',
    },
    {
      image: '/img/templates/proyecto/FORUM/forum-gallery-img-05.png',
      legend: 'La web ofrece además acceso a los consecionarios y sucursales más cercanas al usuario.',
    },
  ]
servicios:
  {
    servicesClass: 'title-center',
    title: 'SERVICIOS UTILIZADOS',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-experiencia.png',
          title: 'EXPERIENCIA DE USUARIO',
          subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
          link: { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link: { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-mantencion.png',
          title: 'MANTENCIÓN',
          subtitle: 'Nuestro equipo de expertos trabaja para mantener y hacer crecer tus plataformas digitales, acercarte a tus clientes y mejorar tus conversiones.',
          link: { text: 'VER DETALLE', url: '/servicios/mantencion/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
