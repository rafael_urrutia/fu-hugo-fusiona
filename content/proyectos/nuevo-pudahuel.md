---
title: Nuevo Pudahuel
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Nuevo Pudahuel']
logo: '/img/pages/proyectos/logos/P_nuevopudahuel_logo@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/NPudahuel/NPud-banner.png',
    bannerClass: 'u-color-gray5',
    minititle: 'Nuevo Pudahuel',
    title: 'Una mantención realmente evolutiva para el aeropuerto más importante de Chile',
    textClass: 'c-banner__text--custom',
    text: '',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Mantenemos los activos digitales de Nuevo Pudahuel para que la información del aeropuerto esté siempre disponible para usuarios y viajeros”',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text: 'Nuevo Pudahuel, consorcio que administra el Aeropuerto Internacional de Santiago, convocó en 2015 a Fusiona para desarrollar la nueva página web del terminal aéreo y su posterior mantención evolutiva. Desde esa fecha hasta hoy se ha producido una creciente demanda, tanto por el aumento de visitas a la web como por la necesidad de informar mejor a pasajeros y usuarios del aeropuerto.',
        },
        {
          title: 'Proceso',
          text:
            'Durante estos años conduciendo la mantención evolutiva del sitio web de Nuevo Pudahuel se han  implementado una serie de mejoras en la tecnología que soporta la página. Por ejemplo, ante el incremento de la demanda se realizó un cambio en la arquitectura de servidores que permite soportar alzas repentinas de visitas, que en períodos específicos puede aumentar hasta en tres veces respecto a lo normal.<br /><br />
            Se hizo una separación entre los servidores: existe uno productivo donde están alojados los contenidos “estáticos” de la web y se creó un segundo específicamente para la consultas relacionadas a los vuelos, información que hoy es mostrada en tiempo real. Además, para ayudar a los usuarios se sumaron nuevos filtros de búsqueda de vuelos, diferenciando entre llegadas, salidas y zonas de embarque, entre otros datos relevantes.<br /><br />
            También se incorporó una funcionalidad que permite hacer seguimiento de la disponibilidad de estacionamientos en el aeropuerto, informando al usuario del número y ubicación de espacios libres, así como de su costo según el número de horas que el usuario lo utilizará.<br /><br />
            Para finalizar, se han implementado una serie de páginas informativas para los usuarios. Entre ellas, un minisite para seguir la reconstrucción del aeropuerto, nuevas secciones que abordan las actividades de RSE de Nuevo Pudahuel (bolsas de trabajo, reducción de la huella de carbono, biblioteca pública digital), zona de prensa y un formulario para declarar objetos perdidos.
            ',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/NPudahuel/NPud-img.png'],
        legend: 'Se creó una página web responsiva para Nuevo Pudahuel, la que puede ser vista correctamente desde cualquier dispositivo de escritorio o móvil.',
      },
    down_content:
      [
        {
          title: 'Resultado',
          text: 'Hoy el sitio web de Nuevo Pudahuel es una plataforma estable y robusta tecnológicamente, con información actualizada en tiempo real y en constante evolución para entregar la mejor experiencia a usuarios y pasajeros del aeropuerto más importante del país. ',
        },
      ],
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/NPudahuel/NPud-gallery-img-01.png',
      legend: 'Como parte del servicio de acompañamiento se creó una funcionalidad para conocer la cantidad y ubicación de los estacionamientos disponibles en el aeropuerto.',
    },
    {
      image: '/img/templates/proyecto/NPudahuel/NPud-gallery-img-02.png',
      legend: 'El sitio web sirve como una plataforma de comunicación donde se informan las noticias más relevantes relacionadas al aeropuerto y a Nuevo Pudahuel.',
    },
    {
      image: '/img/templates/proyecto/NPudahuel/NPud-gallery-img-03.png',
      legend: 'El hogar de la campaña de disminución de la huella de carbono impulsada por Nuevo Pudahuel fue un minisitio creado por Fusiona.',
    },
    {
      image: '/img/templates/proyecto/NPudahuel/NPud-gallery-img-05.png',
      legend: 'Fusiona ha realizado distintas mejoras en la tecnología del sitio para garantizar que la información de vuelos esté siempre actualizada y disponible para los usuarios.',
    },
    {
      image: '/img/templates/proyecto/NPudahuel/NPud-gallery-img-04.png',
      legend: 'La web de Nuevo Pudahuel registra el crecimiento del aeropuerto durante todo el proceso de su ampliación.',
    },
    {
      image: '/img/templates/proyecto/NPudahuel/NPud-gallery-img-06.png',
      legend: 'El minisitio para la campaña de disminución de la huella de carbono permite ver los aportes hechos hasta hoy por los usuarios y sus países de origen.',
    },
  ]
servicios:
  {
    servicesClass: 'title-center',
    title: 'Servicios utilizados',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-mantencion.png',
          title: 'MANTENCIÓN',
          subtitle: 'Nuestro equipo de expertos trabaja para mantener y hacer crecer tus plataformas digitales, acercarte a tus clientes y mejorar tus conversiones.',
          link: { text: 'VER DETALLE', url: '/servicios/mantencion/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-soporte.png',
          title: 'MANEJO DE ACTIVOS TI',
          subtitle: 'Soporte personalizado que permite optimizar, respaldar y actualizar tus plataformas digitales, para que siempre estén disponibles cuando tú o tus clientes las necesiten.',
          link:
            { text: 'VER DETALLE', url: '/servicios/manejo-de-activos-ti/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
