---
title: Enex
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Enex']
logo: '/img/pages/proyectos/logos/p_enex_logo@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/ENEX/Enex-banner.png',
    bannerClass: 'u-color-orange1',
    minititle: 'Enex',
    title: 'Segmentando beneficios para la App Mi Copiloto',
    textClass: 'c-banner__text--custom',
    text:
      'Enex es una de las principales distribuidoras de combustibles y lubricantes en Chile. Opera más de 450 estaciones de servicio bajo la marca Shell, además de más de 160 tiendas de conveniencia bajo el nombre Upa! y Upita. Apostando por la digitalización de las transacciones en las estaciones de servicio, Enex creó la aplicación móvil Mi Copiloto.
      ',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Un estudio de UX que permitió segmentar audiencias y mejorar la evaluación de los clientes”',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text:
            'En el marco de una mejora permanente de su plataformas digitales, Enex buscaba evaluar nuevas funcionalidades para su app Mi Copiloto. A la posibilidad de pagar de forma práctica y segura,  localizar estaciones de servicio Shell o planificar el viaje con Google o Waze, Enex pretendía añadir una línea de beneficios segmentada según tipo de usuario: automovilistas o taxistas.
            ',
        },
        {
          title: 'Proceso',
          text:
            'Se condujeron pruebas de las nuevas funcionalidades en nuestro Laboratorio de Usabilidad para determinar si la información provista gatillaba la acción de ir a un servicentro en busca de la oferta-beneficio. También evaluamos en servicentros Upa! y Upita las nuevas funcionalidades segmentadas para conocer en terreno la recepción de las ofertas y beneficios en los grupos objetivos.
            ',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/ENEX/Enex-img.png'],
        legend: 'Se realizaron una serie de estudios con usuarios reales, tanto en nuestro laboratorio de usabilidad como en las mismas tiendas.',
      },
    down_content: [{ title: 'Resultado', text: '<ul>
            <li>80% de las personas estudiadas evalúo positivamente las nuevas funcionalidades segmentadas, lo que derivó en su incorporación definitiva al aplicativo Mi Copiloto. </li>
            <li>Alto grado de satisfacción de parte del cliente con el testeo de las nuevas funcionalidades, tanto en terreno como en nuestro Laboratorio de Usabilidad.</li>
            </ul>' }],
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/ENEX/Enex-gallery-img-01.png',
      legend: 'La app Mi Copiloto está diseñada para encontrar los servicentros Shell y las tiendas Upa! y Upita más cercanas. ',
    },
    {
      image: '/img/templates/proyecto/ENEX/Enex-gallery-img-02.png',
      legend: 'Entre las funcionalidades destacadas de la app está la billetera, que permite conocer los gastos históricos en combustibles y otros pagos. ',
    },
    {
      image: '/img/templates/proyecto/ENEX/Enex-gallery-img-03.png',
      legend: 'Se realizaron distintas pruebas de UX, entre ellas el uso del eye-tracking, para conocer la disposición de los usuarios a las interfaces propuestas. ',
    },
  ]
servicios:
  {
    servicesClass: 'title-center',
    title: 'Servicios utilizados',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-experiencia.png',
          title: 'EXPERIENCIA DE USUARIO',
          subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
          link: { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
