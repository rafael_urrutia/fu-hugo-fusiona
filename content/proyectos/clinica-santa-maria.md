---
title: Clinica Santa Maria
html_src: templates/proyecto.html
breadcrumbs: ['Inicio', 'Proyectos', 'Clínica Santa Maria']
logo: '/img/pages/proyectos/logos/P_csm_logo@1x.png'
banner:
  {
    type: '4',
    image: '/img/templates/proyecto/CSM/CSM-banner.png',
    bannerClass: 'u-color-green1',
    minititle: 'Clínica Santa María',
    title: 'Creando una experiencia digital de excelencia dentro de la industria de la salud en Chile',
    textClass: 'c-banner__text--custom',
    text: '',
  }
tabs:
  {
    type: '1',
    tabs:
      [{ name: 'Detalle', id: 'detail' }, { name: 'Galería', id: 'gallery' }],
  }
detail_content:
  {
    quote: 'Una nueva y cómoda forma de agendar una hora con tu médico”',
    author: '',
    left_content:
      [
        {
          title: 'Desafío',
          text: 'Clínica Santa María requería modernizar su propuesta digital, creando una vitrina que fuera reflejo del prestigio conseguido en sus casi 90 años como una de las mejores instituciones de salud privada del país. Esto significaba desarrollar plataformas completamente nuevas, con funcionalidades y servicios acordes a las expectativas de pacientes cada vez más exigentes y digitalizados.',
        },
        {
          title: 'Proceso',
          text:
            'El proyecto  incluyó una reingeniería completa de la web y el desarrollo de una aplicación móvil para Android e iOS. En cualquiera de estas plataformas los usuarios pueden conocer y agendar una consulta con los médicos de la Clínica, hacer un seguimiento de todo su historial de salud (así como el de sus cargas), revisar el resultado de sus exámenes generales y de laboratorio, contratar productos, descubrir beneficios y pagar cuentas.<br /><br />
            El trabajo de Experiencia de Usuario y arquitectura de la información fue clave en el proyecto, especialmente para el proceso de toma de horas médicas. Este flujo fue optimizado para que el usuario no gaste más de un minuto y medio desde que ingresa al sitio web o la app hasta que finalmente se confirma su agendamiento con el especialista seleccionado.<br /><br />
            Fusiona ha continuado prestando servicios de mantención evolutiva para la Clínica. Especialmente luego de la pandemia de Covid 19, el sitio ha sido reforzado tanto en su infraestructura tecnológica como en el set de funcionalidades que ofrece a los pacientes. Se integró una plataforma de Telemedicina dentro del sitio web, se levantaron páginas para informar de los nuevos protocolos de atención y se creó un flujo para autentificar de forma online a los pacientes que deseen enrolarse como clientes de la Clínica.
            ',
        },
      ],
    right_content:
      {
        images: ['/img/templates/proyecto/CSM/CSM-img.png'],
        legend: 'El sitio web se construyó privilegiando una conversión principal: la solicitud de horas médicas.',
      },
    down_content:
      [
        {
          title: 'Resultado',
          text: 'La web y la aplicación móvil de Clínica Santa María están hoy a la punta en la industria de la salud en Chile, tanto por la calidad de la experiencia que obtienen los usuarios como por la robustez tecnológica de ambas plataformas. Hoy cerca de un 50% de todas las reservas de horas médicas de la clínica se agendan a través de la app y la página web creadas por Fusiona.',
        },
      ],
  }
gallery:
  [
    {
      image: '/img/templates/proyecto/CSM/CSM-gallery-img-01.png',
      legend: 'El sitio incluye un completo detalle de las áreas médicas de la Clínica.',
    },
    {
      image: '/img/templates/proyecto/CSM/CSM-gallery-img-02.png',
      legend: 'La aplicación de la Clínica ofrece una navegación simplificada para mejorar la usabilidad.',
    },
    {
      image: '/img/templates/proyecto/CSM/CSM-gallery-img-03.png',
      legend: 'La página web es constantemente actualizada para ofrecer información útil y de calidad para los pacientes.',
    },
    {
      image: '/img/templates/proyecto/CSM/CSM-gallery-img-06.png',
      legend: 'El proceso de solicitud de horas se optimiza de forma permanente para hacer la experiencia de uso más cómoda. ',
    },
    {
      image: '/img/templates/proyecto/CSM/CSM-gallery-img-05.png',
      legend: 'La aplicación de la Clínica permite crear un perfil personalizado con información confidencial del cliente y sus cargas. ',
    },
    {
      image: '/img/templates/proyecto/CSM/CSM-gallery-img-04.png',
      legend: 'Tanto la página web como la app tienen como conversión central la reserva de horas médicas.',
    },
    {
      image: '/img/templates/proyecto/CSM/CSM-gallery-img-07.png',
      legend: 'La app permite pagar cuentas, revisar exámenes y conocer convenios especiales, entre otras funcionalidades ',
    },
  ]
servicios:
  {
    servicesClass: 'title-center',
    title: 'Servicios utilizados',
    subtitle: '',
    slider:
      [
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-experiencia.png',
          title: 'EXPERIENCIA DE USUARIO',
          subtitle: 'Mejoramos la experiencia de usuarios y clientes usando metodologías probadas y nuestro laboratorio UX especializado.',
          link:
            { text: 'VER DETALLE', url: '/servicios/experiencia-de-usuario/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-desarrollo.png',
          title: 'DESARROLLO Y TECNOLOGÍA',
          subtitle: 'Creamos soluciones tecnológicas escalables y de alto rendimiento que te ayudarán a innovar y acelerar la transformación digital de tu compañía.',
          link:
            { text: 'VER DETALLE', url: '/servicios/desarrollo-y-tecnologia/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-mantencion.png',
          title: 'MANTENCIÓN',
          subtitle: 'Nuestro equipo de expertos trabaja para mantener y hacer crecer tus plataformas digitales, acercarte a tus clientes y mejorar tus conversiones.',
          link: { text: 'VER DETALLE', url: '/servicios/mantencion/' },
        },
        {
          type: '1',
          image: '/img/pages/home/servicios/ico-soporte.png',
          title: 'MANEJO DE ACTIVOS TI',
          subtitle: 'Soporte personalizado que permite optimizar, respaldar y actualizar tus plataformas digitales, para que siempre estén disponibles cuando tú o tus clientes las necesiten.',
          link:
            { text: 'VER DETALLE', url: '/servicios/manejo-de-activos-ti/' },
        },
      ],
  }
proyectos:
  {
    title: 'Proyectos relacionados',
    cards:
      [
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-movistar.png',
          title: 'Movistar',
          subtitle: 'Mejorando el viaje del consumidor digital',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-orange1',
          image: '/img/commons/proyectos/img-small-kaufmann.png',
          title: 'Kaufmann',
          subtitle: 'Optimización de la conversión',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-green1',
          image: '/img/commons/proyectos/img-small-santander.png',
          title: 'Santander',
          subtitle: 'Gestionar pedidos de una manera más sencilla, rápida y cómoda',
          link: { text: 'VER PROYECTO', url: '' },
        },
        {
          type: '3',
          heightClass: 'c-cards__projects--small-card',
          cardClass: 'u-color-purple3',
          image: '/img/commons/proyectos/img-small-teleton.png',
          title: 'Banco de chile',
          subtitle: 'Minisitio Teletón 2018, historia y tecnología.',
          link: { text: 'VER PROYECTO', url: '' },
        },
      ],
  }
listo:
  {
    title: '¿LISTO PARA INICIAR TU PROYECTO?',
    subtitle: 'Ven a conocer toda la variedad de servciios que ofrecemos para ayudarte',
    link: { text: 'CONTÁCTANOS', url: '' },
  }
css_src: ['/css/slick.css', '/css/slick-theme.css']
js_src: []
---
