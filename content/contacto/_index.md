---
title: Contacto
html_src: pages/contacto.html
breadcrumbs: ['Inicio', 'Contacto']
banner:
  {
    type: '1',
    title: 'contacto',
    titleClass: 'u-text-center',
    subtitle: '¿Listo para iniciar tu proyecto?',
    subtitleClass: 'u-text-center',
  }
contact: { title: 'Contáctanos ahora' }
tabs:
  {
    type: '2',
    tabs:
      [
        { name: 'Chile', id: 'chile-contact' },
        { name: 'Colombia', id: 'colombia-contact' },
      ],
  }

form: { title: 'Trabajemos juntos', text: '' }
css_src: []
js_src: []
---
